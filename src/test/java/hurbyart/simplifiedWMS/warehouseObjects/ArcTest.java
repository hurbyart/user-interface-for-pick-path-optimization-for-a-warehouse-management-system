package hurbyart.simplifiedWMS.warehouseObjects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArcTest {


    @Test
    public void calculateTravelFactor_workingCorrect(){
        double expectedTravelFactor = 70.71067811865476;
        NodeWMS node1 = new NodeWMS(0,100,100,null,0);
        NodeWMS node2 = new NodeWMS(0,150,150,null,0);;

        Arc arc = new Arc(0,node1,node2,0);
        double realTravelFactor = arc.getTravelFactor();

        assertEquals(expectedTravelFactor, realTravelFactor);

    }
}