package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class ArcGraphics implements Graphics {

    private Arc arc;
    private Line line;

    public ArcGraphics(Arc arc, Line line) {
        this.arc = arc;
        this.line = line;
        line.setStroke(Color.BLACK);

    }

    /**
     * Method for creating Tooltip(pop up) with arc length
     */
    public void setTooltip() {
        Tooltip.install(line, new Tooltip(String.format("%.2f m", arc.getTravelFactor() / 10)));
    }

    public Arc getArc() {
        return arc;
    }

    public Line getLine() {
        return line;
    }

    public boolean isInside(double x, double y) {
        return false;
    }

}
