package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Rack;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.List;

public class RackGraphics implements Graphics {

    public final static int RACK_STROKE_SIZE = 1;

    private Rack rack;
    private Rectangle rectangle;
    private List<Line> lines;
    private boolean selected = false;


    public RackGraphics(Rack rack, Rectangle rectangle) {
        this.rack = rack;
        this.rectangle = rectangle;
        rectangle.setFill(Color.DODGERBLUE);
        rectangle.setStroke(Color.BLACK);
        rectangle.setStrokeWidth(RACK_STROKE_SIZE);
        addLines();
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public List<Shape> getRectangleAndLines() {
        List<Shape> graphics = new ArrayList<>();
        graphics.add(rectangle);
        graphics.addAll(lines);
        return graphics;
    }


    public List<Shape> getNodesAndLineGraphic() {
        List<Shape> ret = new ArrayList<>();
        for (Arc arc : rack.getArcs()) {
            ret.add(arc.getGraphics().getLine());
        }
        for (NodeWMS node : rack.getNodes()) {
            ret.add(node.getGraphics().getCircle());
        }
        return ret;
    }

    public List<Line> getLines() {
        return lines;
    }


    public boolean isSelected() {
        return selected;
    }

    /**
     * Method for correct changing rack horizontal coordinate
     */
    public double moveX(double x) {
        double change = rectangle.getX() - x;
        rectangle.setX(x);
        for (Line line : lines) {
            line.setStartX(line.getStartX() - change);
            line.setEndX(line.getEndX() - change);
        }
        return change;
    }

    /**
     * Method for correct changing rack vertical coordinate
     */
    public double moveY(double y) {
        double change = rectangle.getY() - y;
        rectangle.setY(y);
        for (Line line : lines) {
            line.setStartY(line.getStartY() - change);
            line.setEndY(line.getEndY() - change);
        }

        return change;
    }

    private void addLines() {
        this.lines = new ArrayList<>();
        if (rack.isHorizontal()) {
            for (int i = 1; i < rack.getNumberOfSlots(); i++) {
                Line line = new Line(rectangle.getX() + i * rack.getSlotSize(), rectangle.getY() + RACK_STROKE_SIZE, rectangle.getX() + i * rack.getSlotSize(), rectangle.getY() + rack.getRackSizeY() - 2 * RACK_STROKE_SIZE);
                line.setStroke(Color.DARKGRAY);
                lines.add(line);
            }
            if (rack.isTwoSlots()) {
                Line lastLine = new Line(rectangle.getX() + RACK_STROKE_SIZE, rectangle.getY() + rack.getRackSizeY() / 2, rectangle.getX() + rack.getRackSizeX() - 2 * RACK_STROKE_SIZE, rectangle.getY() + rack.getRackSizeY() / 2);
                lastLine.setStroke(Color.DARKGRAY);
                lines.add(lastLine);
            }
        } else {
            for (int i = 1; i < rack.getNumberOfSlots(); i++) {
                Line line = new Line(rectangle.getX() + RACK_STROKE_SIZE, rectangle.getY() + i * rack.getSlotSize(), rectangle.getX() + rack.getRackSizeX() - 2 * RACK_STROKE_SIZE, rectangle.getY() + i * rack.getSlotSize());
                line.setStroke(Color.DARKGRAY);
                lines.add(line);
            }
            if (rack.isTwoSlots()) {
                Line lastLine = new Line(rectangle.getX() + rack.getRackSizeX() / 2, rectangle.getY() + RACK_STROKE_SIZE, rectangle.getX() + rack.getRackSizeX() / 2, rectangle.getY() + rack.getRackSizeY() - 2 * RACK_STROKE_SIZE);
                lastLine.setStroke(Color.DARKGRAY);
                lines.add(lastLine);
            }

        }
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) rectangle.setFill(Color.YELLOW);
        else rectangle.setFill(Color.DODGERBLUE);
    }

    public void setFill(Color color) {
        rectangle.setFill(color);
    }


    public boolean isInside(double x, double y) {
        return
                x >= rectangle.getX() && x <= rectangle.getX() + rack.getRackSizeX() &&
                        y >= rectangle.getY() && y <= rectangle.getY() + rack.getRackSizeY();
    }
}
