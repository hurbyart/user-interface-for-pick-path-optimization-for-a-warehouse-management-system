package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

import hurbyart.simplifiedWMS.db.GetDatabase;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


public class StartingPointGraphics extends NodeGraphics implements Graphics {

    private final double STARTING_POINT_RADIUS = 4;


    public StartingPointGraphics() {
        super.circle = new Circle();
        super.circle.setStroke(Color.BLUE);
        Double[] coordinates = GetDatabase.getStartingPointCoordinates();
        if (coordinates != null && coordinates[0] != null) {
            super.circle.setCenterX(coordinates[0]);
            super.circle.setCenterY(coordinates[1]);
        } else {
            super.circle.setCenterX(0);
            super.circle.setCenterY(0);
        }
        super.circle.setRadius(STARTING_POINT_RADIUS);
    }


}
