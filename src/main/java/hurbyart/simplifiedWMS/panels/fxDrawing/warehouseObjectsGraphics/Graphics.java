package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

public interface Graphics {

    /**
     * Method for checking if click was inside object
     *
     * @param x
     * @param y
     * @return true if was inside, false otherwise
     */
    boolean isInside(double x, double y);
}
