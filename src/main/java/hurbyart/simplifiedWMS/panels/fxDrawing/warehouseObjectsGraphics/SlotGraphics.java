package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import hurbyart.simplifiedWMS.warehouseObjects.Slot;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.List;

public class SlotGraphics implements Graphics {

    private Slot slot;
    private Rectangle rectangle;
    private Tooltip tooltip;

    public SlotGraphics(Slot slot, Rectangle rectangle) {
        this.slot = slot;
        this.rectangle = rectangle;
        rectangle.setStroke(Color.DARKGRAY);
    }


    public void resetTooltip() {
        Tooltip.uninstall(rectangle, tooltip);
        Tooltip.install(rectangle, createTooltip());
    }

    public void setTooltip() {
        Tooltip.install(rectangle, createTooltip());
    }

    /**
     * Method for creating Tooltip(pop up) with information about slot and SKU in the slot
     *
     * @return tooltip
     */
    private Tooltip createTooltip() {
        String text = "Slot information\n" + "Slot " + slot.getRackId() + "-" + slot.getId() + "\n";

        Sku skuFromsSlot = GetDatabase.getOneSKUInSlot(slot.getId());

        if (skuFromsSlot == null) {
            text += "Slot is empty! No SKU info to show!" + "\n";
        } else {
            text += "SKU name: " + skuFromsSlot.getName() + "\n"
                    + "SKU description: " + skuFromsSlot.getDescription() + "\n"
                    + "SKU amount in one pallet: " + skuFromsSlot.getAmountInOnePallet() + "\n"
                    + "SKU width: " + skuFromsSlot.getWidth() + "\n"
                    + "SKU length: " + skuFromsSlot.getLength() + "\n"
                    + "SKU height: " + skuFromsSlot.getHeight() + "\n";
        }
        return new Tooltip(text);
    }

    public Rectangle getRectangle() {
        checkColor();
        return rectangle;
    }


    public boolean isInside(double x, double y) {
        return
                x >= rectangle.getX() && x <= rectangle.getX() + slot.getSlotSizeX() &&
                        y >= rectangle.getY() && y <= rectangle.getY() + slot.getSlotSizeY();
    }


    /**
     * Method for checking and changing slot color
     * LIGHTGREEN - slot is empty
     * YELLOW - there is some SKU in the slot
     */
    public void checkColor() {
        List<Sku> skus = GetDatabase.getAllSKUInSlot(slot.getId());
        if (skus != null && !skus.isEmpty()) rectangle.setFill(Color.YELLOW);
        else rectangle.setFill(Color.LIGHTGREEN);
    }

    public void setSelected(boolean selected) {
        if (selected) rectangle.setFill(Color.GREEN);
        else checkColor();
    }
}
