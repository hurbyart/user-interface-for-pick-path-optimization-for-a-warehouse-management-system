package hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics;

import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


public class NodeGraphics implements Graphics {

    public final static double NODE_RADIUS = 2.5;

    private NodeWMS node;
    protected Circle circle;
    protected boolean selected;

    public NodeGraphics(NodeWMS node, Circle circle) {
        this.node = node;
        this.circle = circle;
        this.selected = false;
    }

    public NodeGraphics() {
    }

    /**
     * Method for creating tooltip(pop up) with node coordinates
     */
    public void setTooltip() {
        Tooltip.install(circle, new Tooltip("x: " + circle.getCenterX() / 10 + " m\ny: " + circle.getCenterY() / 10 + " m"));
    }

    public Circle getCircle() {
        return circle;
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isInside(double x, double y) {
        return
                x >= circle.getCenterX() - NODE_RADIUS && x <= circle.getCenterX() + NODE_RADIUS &&
                        y >= circle.getCenterY() - NODE_RADIUS && y <= circle.getCenterY() + NODE_RADIUS;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            circle.setFill(Color.YELLOW);
        } else {
            if (this instanceof StartingPointGraphics) circle.setFill(Color.BLACK);
            else circle.setFill(Color.DARKBLUE);
        }
    }
}
