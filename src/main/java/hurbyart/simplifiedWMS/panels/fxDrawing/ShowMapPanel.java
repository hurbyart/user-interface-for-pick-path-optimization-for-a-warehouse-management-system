package hurbyart.simplifiedWMS.panels.fxDrawing;

import hurbyart.simplifiedWMS.algorithms.Algorithm;
import hurbyart.simplifiedWMS.algorithms.dijkstra.DijkstraAlgorithm;
import hurbyart.simplifiedWMS.algorithms.dijkstra.DijkstraAlgorithmWithPermutation;
import hurbyart.simplifiedWMS.algorithms.psjava.PSJavaAlgorithm;
import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.dialogs.AddingNewSKUToSlotDialog;
import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.Panels;
import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.StartingPointGraphics;
import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import hurbyart.simplifiedWMS.warehouseObjects.Slot;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.util.Pair;

import javax.swing.*;
import java.util.*;


import static hurbyart.simplifiedWMS.panels.fxDrawing.MapCreationMainPanel.*;
import static hurbyart.simplifiedWMS.util.AlgorithmDurationLogger.ALGORITHM_DURATION_LOGGER;

public class ShowMapPanel extends Panels {

    private final static boolean USE_RANDOM_COLOR = false;

    JFrame frame;
    private double warehouseWidth;
    private double warehouseLength;
    List<Slot> slots;
    List<NodeWMS> nodes;
    List<Arc> arcs;
    AddingNewSKUToSlotDialog addNewSKUToSLot;
    PannablePane pPane;
    private boolean selectionMode = false;
    private UpperPaneShowMap upperPane;
    private List<Shape> drawnPath = new ArrayList<>();
    private final Color[] colors = {Color.BLACK, Color.GREEN, Color.RED, Color.YELLOW, Color.BROWN, Color.ORANGE, Color.VIOLET, Color.PINK, Color.MAGENTA, Color.LIGHTGREEN};
    private Map<Line, Integer> copies = new HashMap<>();
    private NodeWMS spNode;
    private List<List<Arc>> fullPath = new ArrayList<>();

    public ShowMapPanel(JFrame frame) {
        this.frame = frame;
        this.addNewSKUToSLot = new AddingNewSKUToSlotDialog(this);
        double[] warehouseSizes = GetDatabase.getWarehouseSize();
        if (warehouseSizes == null) {
            this.warehouseWidth = 0;
            this.warehouseLength = 0;
        } else {
            this.warehouseWidth = warehouseSizes[0];
            this.warehouseLength = warehouseSizes[1];
        }
        loadArcsAndNodes();
    }

    public List<List<Arc>> getFullPath() {
        return fullPath;
    }

    public void loadArcsAndNodes() {
        this.nodes = GetDatabase.getAllNodes();
        this.arcs = GetDatabase.getAllArcs();

        StartingPointGraphics spg = new StartingPointGraphics();
        spNode = new NodeWMS(spg);
        nodes.add(spNode);
        for (Arc arc : arcs) {
            for (NodeWMS n : nodes) {
                if (arc.getNode1().equals(n)) n.getNeighbours().put(arc.getNode2(), arc.getTravelFactor());
                if (arc.getNode2().equals(n)) n.getNeighbours().put(arc.getNode1(), arc.getTravelFactor());
            }
        }

    }

    /**
     * Method for starting JavaFX panel
     */
    public void act() {
        frame.getContentPane().removeAll();
        frame.getContentPane().invalidate();


        JFXPanel fxPanel = new JFXPanel();


        frame.add(fxPanel);
        frame.setVisible(true);
        Platform.runLater(() -> {
            Scene scene = createScene();
            fxPanel.setScene(scene);
        });
        frame.getContentPane().revalidate();
    }

    /**
     * Show plan panel behavior method
     */
    private Scene createScene() {

        Group group = new Group();
        this.slots = GetDatabase.getAllSlots();
        this.pPane = new PannablePane(warehouseWidth, warehouseLength);
        this.upperPane = new UpperPaneShowMap(this, WINDOW_WIDTH, UPPER_MENU_HEIGHT);

        fakeAlertToActiveFXListeners();

        group.getChildren().add(pPane);
        group.getChildren().add(upperPane);
        // center it
        pPane.setTranslateX(-(warehouseWidth - WINDOW_WIDTH) / 2);
        pPane.setTranslateY(-(warehouseLength - WINDOW_HEIGHT) / 2);

        // create scene which can be dragged and zoomed
        Scene scene = new Scene(group, WINDOW_WIDTH, WINDOW_HEIGHT);

        SceneGestures sceneGestures = new SceneGestures(pPane);
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, sceneGestures.getOnMousePressedEventHandler());
        scene.addEventFilter(MouseEvent.MOUSE_DRAGGED, sceneGestures.getOnMouseDraggedEventHandler());
        scene.addEventFilter(ScrollEvent.ANY, sceneGestures.getOnScrollEventHandler());


        for (Slot slot : slots) {
            slot.getGraphics().setTooltip();
            pPane.getChildren().add(slot.getGraphics().getRectangle());
        }

        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case F1:
                    upperPane.showUserGuide();
                    break;
                case ESCAPE:
                    PanelController.start(PanelMode.MENU);
                    break;
                case CONTROL:
                    selectionMode = true;
                    break;
                case C:
                    erasePath();
                    deletePath();
                    upperPane.deleteAlgorithmDurationLabel();
                    break;
            }
        });

        scene.setOnKeyReleased(e -> {
            if (e.getCode() == KeyCode.CONTROL) {
                selectionMode = false;
            }
        });

        pPane.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown()) {
                Slot slot = getSlotOnThisSpot(event.getX(), event.getY());
                if (slot == null) return;
                if (selectionMode) {
                    Sku sku = GetDatabase.getOneSKUInSlot(slot.getId());
                    if (sku == null) return;
                    upperPane.getDialog().getSkusInOrder().add(sku);
                    for (Slot s : slots) {
                        if (sku.getSlotId() == s.getId()) s.getGraphics().getRectangle().setFill(Color.LIGHTYELLOW);
                    }
                } else {
                    slot.getGraphics().setSelected(true);
                    addNewSKUToSLot.act(slot);
                    slot.getGraphics().setSelected(false);
                    slot.getGraphics().resetTooltip();
                }
            }
        });

        return scene;
    }


    private void fakeAlertToActiveFXListeners() {
        Alert a = new Alert(Alert.AlertType.WARNING, "");
        a.show();
        a.close();
    }

    private void erasePath() {
        upperPane.deletePathLengthLabel();
        pPane.getChildren().removeAll(drawnPath);
        copies.clear();
    }

    public void changeColorOfSlot(int slotId, Color color) {
        for (Slot slot : slots) {
            if (slotId == slot.getId()) {
                slot.getGraphics().getRectangle().setFill(color);
                return;
            }
        }
    }

    public void returnColorOfSlot(int slotId) {
        for (Slot slot : slots) {
            if (slotId == slot.getId()) {
                slot.getGraphics().checkColor();
                return;
            }
        }
    }

    private Slot getSlotOnThisSpot(double x, double y) {
        for (Slot slot : slots) {
            if (slot.getGraphics().isInside(x, y)) {
                return slot;
            }
        }
        return null;
    }


    private void writeDurationToLogFile(long time, String algorithmName, double pathLength) {
        String pathLengthStr = String.format("%.2f", pathLength);
        ALGORITHM_DURATION_LOGGER.info("Path with length " + pathLengthStr + " m was created by  " +
                algorithmName + " algorithm in " + time + " ms.");
    }

    /**
     * Method for creating pick path using some algorithm
     *
     * @param skus         -- list of skus in the order
     * @param useSpAsStart -- if use starting point as start of pick path
     * @param useSpAsEnd   -- if use starting point as end of pick path
     * @param algStr       -- name of algorithm
     */
    public void useAlgorithm(List<Sku> skus, boolean useSpAsStart, boolean useSpAsEnd, String algStr) {
        loadArcsAndNodes();
        double pathLength;
        long startAlgorithmTime = System.nanoTime();


        Algorithm algorithm;
        switch (algStr) {
            case "Dijkstra":
                algorithm = new DijkstraAlgorithm(spNode, nodes, arcs);
                break;
            case "DijkstraPerm":
                algorithm = new DijkstraAlgorithmWithPermutation(spNode, nodes, arcs);
                break;
            case "Johnson":
                algorithm = new PSJavaAlgorithm(spNode, nodes, arcs, true);
                break;
            case "Floyd-Warshall":
                algorithm = new PSJavaAlgorithm(spNode, nodes, arcs, false);
                break;
            default:
                System.out.println("Algorithm " + algStr + " is not implemented");
                return;
        }

        deletePath();
        Pair<List<List<Arc>>, Double> result = algorithm.createPath(skus, useSpAsStart, useSpAsEnd);
        if (result == null || result.getValue() >= Double.MAX_VALUE) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Path was not found.\nMaybe there is no path between some SKU slots.");
            alert.showAndWait();
            return;
        }
        pathLength = result.getValue();
        long duration = (System.nanoTime() - startAlgorithmTime) / 1000000; // dividing to convert to ms
        upperPane.deleteAlgorithmDurationLabel();
        if (result.getKey().size() > 0) {
            pathLength = pathLength / 10;
            this.fullPath = result.getKey();
            upperPane.activatePathPartFieldAndButtons(fullPath.size());
            writeDurationToLogFile(duration, algStr, pathLength);
            draw(fullPath, pathLength);
            upperPane.setAlgorithmDuration(duration);
        } else {
            upperPane.deletePathLengthLabel();
            Alert alert = new Alert(Alert.AlertType.WARNING, "Path is empty!");
            alert.showAndWait();
        }
    }


    private void deletePath() {
        this.fullPath = null;
        upperPane.getPathPartField().setText("");
        upperPane.getRightArrow().setDisable(true);
    }

    /**
     * Method for drawing pick path with given arcs and setting labels at Show plan panel
     *
     * @param arcsToDraw
     * @param length
     */
    public void draw(List<List<Arc>> arcsToDraw, double length) {
        erasePath();
        Set<NodeWMS> nodesToDraw = new HashSet<>();
        for (int i = 0; i < arcsToDraw.size(); i++) {
            Color color;
            if (arcsToDraw.size() <= colors.length) color = USE_RANDOM_COLOR ? randomColor() : colors[i];
            else color = randomColor();
            List<Arc> l = arcsToDraw.get(i);
            for (Arc arc : l) {
                if (pPane.getChildren().contains(arc.getGraphics().getLine())) {
                    int counter = 1;

                    for (int v : copies.values()) {
                        if (v == arc.getId()) {
                            counter++;
                        }
                    }
                    counter = covertAmountOfCopiesToCopysPosition(counter);
                    Line copy = new Line();
                    copies.put(copy, arc.getId());
                    copy.setStroke(color);
                    if (Math.abs(arc.getGraphics().getLine().getStartX() - arc.getGraphics().getLine().getEndX()) <
                            Math.abs(arc.getGraphics().getLine().getStartY() - arc.getGraphics().getLine().getEndY())) {
                        copy.setStartX(arc.getGraphics().getLine().getStartX() + arc.getGraphics().getLine().getStrokeWidth() * counter);
                        copy.setEndX(arc.getGraphics().getLine().getEndX() + arc.getGraphics().getLine().getStrokeWidth() * counter);
                        copy.setStartY(arc.getGraphics().getLine().getStartY());
                        copy.setEndY(arc.getGraphics().getLine().getEndY());
                    } else {
                        copy.setStartX(arc.getGraphics().getLine().getStartX());
                        copy.setEndX(arc.getGraphics().getLine().getEndX());
                        copy.setStartY(arc.getGraphics().getLine().getStartY() + arc.getGraphics().getLine().getStrokeWidth() * counter);
                        copy.setEndY(arc.getGraphics().getLine().getEndY() + arc.getGraphics().getLine().getStrokeWidth() * counter);
                    }
                    pPane.getChildren().add(copy);
                    drawnPath.add(copy);
                } else {
                    arc.getGraphics().getLine().setStroke(color);
                    pPane.getChildren().add(arc.getGraphics().getLine());
                    nodesToDraw.add(arc.getNode1());
                    nodesToDraw.add(arc.getNode2());
                    drawnPath.add(arc.getGraphics().getLine());
                }
            }

        }
        for (NodeWMS n : nodesToDraw) {
            pPane.getChildren().add(n.getGraphics().getCircle());
            drawnPath.add(n.getGraphics().getCircle());
        }

        upperPane.createPathLengthsLabel(length);
    }


    private Color randomColor() {
        int r = (int) (Math.random() * 255);
        int g = (int) (Math.random() * 255);
        int b = (int) (Math.random() * 255);
        return Color.rgb(r, g, b);
    }


    private int covertAmountOfCopiesToCopysPosition(int v) {
        if (v == 1) return 1;
        if (v % 2 == 0) return -(v / 2);
        else return (v + 1) / 2;
    }

}
