package hurbyart.simplifiedWMS.panels.fxDrawing;


import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.fxDrawing.dialogs.CreatingOrderDialog;
import hurbyart.simplifiedWMS.util.RangeParser;

import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import javafx.geometry.Pos;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;


public class UpperPaneShowMap extends Pane {

    private static final String HELP_IMAGE = "help_button_icon.png";
    private static final String LEFT_ARROW_IMAGE = "left_arrow_icon.png";
    private static final String RIGHT_ARROW_IMAGE = "right_arrow_icon.png";


    private Button helpButton;
    private Button checkSkuInOrder;
    private CreatingOrderDialog dialog;
    private ShowMapPanel showMapPanel;
    private Label pathLength;
    private TextField pathPartField;
    private Button leftArrow;
    private Button rightArrow;
    private Label algorithmDuration;
    private Integer pathNumber = 0;

    public UpperPaneShowMap(ShowMapPanel showMapPanel, double width, double height) {
        this.showMapPanel = showMapPanel;

        setStyle("-fx-background-color: #4085a2; -fx-border-color: orange;");
        setPrefSize(width, height);

        this.pathPartField = createPathPartField();
        this.checkSkuInOrder = createCheckSkuInOrderButton();
        this.helpButton = createHelpButton();
        this.leftArrow = createLeftArrow();
        this.rightArrow = createRightArrow();


        this.getChildren().add(pathPartField);
        this.getChildren().add(checkSkuInOrder);
        this.getChildren().add(helpButton);
        this.getChildren().add(leftArrow);
        this.getChildren().add(rightArrow);

        this.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case ESCAPE:
                    PanelController.start(PanelMode.MENU);
                    break;
            }
        });
    }

    /**
     * Method for creating label with calculation duration using some algorithm
     *
     * @param time
     */
    public void setAlgorithmDuration(long time) {
        String text = time > 9999 ? "Alg. duration: " + time / 1000 + " s" : "Alg. duration: " + time + " ms";
        Label algDurationLabel = new Label(text);
        algDurationLabel.setTranslateX(300);
        algDurationLabel.setTranslateY(7);
        algDurationLabel.setFont(new Font(20));
        this.algorithmDuration = algDurationLabel;
        this.getChildren().add(algDurationLabel);
    }


    public ShowMapPanel getShowMapPanel() {
        return showMapPanel;
    }

    public CreatingOrderDialog getDialog() {
        return dialog;
    }

    public TextField getPathPartField() {
        return pathPartField;
    }

    public Button getRightArrow() {
        return rightArrow;
    }

    private Button createCheckSkuInOrderButton() {
        this.dialog = new CreatingOrderDialog(this);

        Button button = new Button();
        button.setTranslateX(540);
        button.setTranslateY(7);
        button.setText("Show SKUs in order");
        button.setOnAction(e -> {
            dialog.act();
        });
        return button;
    }

    private Button createLeftArrow() {
        Image image = new Image(LEFT_ARROW_IMAGE);
        ImageView view = new ImageView(image);
        view.setFitHeight(20);
        view.setFitWidth(20);
        view.setPreserveRatio(true);
        Button button = new Button();
        button.setDisable(true);
        button.setTranslateX(703);
        button.setTranslateY(7);
        button.setAlignment(Pos.BASELINE_CENTER);
        button.setGraphic(view);

        button.setOnAction(e -> {
            rightArrow.setDisable(false);
            if (pathNumber == null) {
                pathNumberNullAlert();
                return;
            }
            pathNumber--;
            if (pathNumber == 1) button.setDisable(true);
            pathPartField.setText(pathNumber.toString());
            List<List<Arc>> toDraw = new ArrayList<>();
            toDraw.add(showMapPanel.getFullPath().get(pathNumber - 1));
            showMapPanel.draw(toDraw, calculatePathLength(toDraw));
        });
        return button;
    }

    private double calculatePathLength(List<List<Arc>> arcs) {
        double length = 0.0;
        for (List<Arc> l : arcs) {
            for (Arc arc : l) {
                length += arc.getTravelFactor();
            }
        }
        return length / 10;
    }

    private Button createRightArrow() {
        Image image = new Image(RIGHT_ARROW_IMAGE);
        ImageView view = new ImageView(image);
        view.setFitHeight(20);
        view.setFitWidth(20);
        view.setPreserveRatio(true);
        Button button = new Button();
        button.setDisable(true);
        button.setTranslateX(930);
        button.setTranslateY(7);
        button.setAlignment(Pos.BASELINE_CENTER);
        button.setGraphic(view);
        button.setOnAction(e -> {
            if (pathNumber != 0) leftArrow.setDisable(false);
            if (pathNumber == null) {
                pathNumberNullAlert();
                return;
            }
            pathNumber++;
            List<List<Arc>> fullPath = showMapPanel.getFullPath();
            if (pathNumber == fullPath.size()) button.setDisable(true);
            pathPartField.setText(pathNumber.toString());
            List<List<Arc>> toDraw = new ArrayList<>();
            toDraw.add(showMapPanel.getFullPath().get(pathNumber - 1));
            showMapPanel.draw(toDraw, calculatePathLength(toDraw));
        });
        return button;
    }

    private void pathNumberNullAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setHeaderText(null);
        alert.setContentText("To use arrows set choose only one path part!");
        alert.showAndWait();
    }

    public void activatePathPartFieldAndButtons(int size) {
        if (size > 1) {
            getPathPartField().setText("1-" + size);
            rightArrow.setDisable(false);
            pathPartField.setEditable(true);
        }
        if (size == 1) getPathPartField().setText("1");
    }

    public void createPathLengthsLabel(double length) {
        Label pathLength = new Label("Path Length: " + String.format("%.2f", length) + " m");
        pathLength.setTranslateY(7);
        pathLength.setTranslateX(5);
        pathLength.setFont(new Font(20));
        this.pathLength = pathLength;
        this.getChildren().add(pathLength);
    }

    public void deletePathLengthLabel() {
        this.getChildren().remove(pathLength);
    }

    public void deleteAlgorithmDurationLabel() {
        this.getChildren().remove(algorithmDuration);
        this.pathNumber = 0;
    }

    private TextField createPathPartField() {
        this.pathPartField = new TextField();
        pathPartField.setEditable(false);
        pathPartField.setTranslateX(750);
        pathPartField.setTranslateY(7);
        pathPartField.setPromptText("Path parts(1, 3, 5-7)");
        pathPartField.setTooltip(new Tooltip("Path parts(1, 3, 5-7)\nPath part field is active only when path is shown."));
        pathPartField.setAlignment(Pos.BASELINE_CENTER);
        pathPartField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (pathPartField.getText().equals("all") || pathPartField.getText().equals("All")) {
                    pathNumber = 0;
                    rightArrow.setDisable(false);
                    leftArrow.setDisable(true);
                    List<List<Arc>> fullPath = showMapPanel.getFullPath();
                    pathPartField.setText("1-" + fullPath.size());
                    showMapPanel.draw(showMapPanel.getFullPath(), calculatePathLength(showMapPanel.getFullPath()));
                } else {
                    List<Integer> parts = RangeParser.getDistinctNumbers(pathPartField.getText());
                    List<List<Arc>> fullPath = showMapPanel.getFullPath();
                    if (fullPath.size() == 0) {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Alert");
                        alert.setHeaderText(null);
                        alert.setContentText("The path wasn't drawn yet!");
                        alert.showAndWait();
                        return;
                    }
                    List<List<Arc>> chosenParts = new ArrayList<>();
                    for (Integer n : parts) {
                        try {
                            chosenParts.add(fullPath.get(n - 1));
                        } catch (IndexOutOfBoundsException ex) {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Alert");
                            alert.setHeaderText(null);
                            alert.setContentText("Path part number is too big!");
                            alert.showAndWait();
                            return;
                        }
                    }
                    if (parts.size() == 1) {
                        leftArrow.setDisable(false);
                        rightArrow.setDisable(false);
                        pathNumber = parts.get(0);
                        if (pathNumber == 1) leftArrow.setDisable(true);
                        if (pathNumber == fullPath.size()) rightArrow.setDisable(true);
                        pathPartField.setText(pathNumber.toString());

                    } else {
                        leftArrow.setDisable(true);
                        rightArrow.setDisable(true);
                        pathNumber = null;
                    }
                    showMapPanel.draw(chosenParts, calculatePathLength(chosenParts));
                }
            }
        });
        return pathPartField;
    }

    private Button createHelpButton() {
        Image img = new Image(HELP_IMAGE);
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setFitWidth(20);
        view.setPreserveRatio(true);

        Button button = new Button();
        button.setTranslateX(1040);
        button.setTranslateY(5);
        button.setGraphic(view);

        button.setOnAction(e -> {
            showUserGuide();
        });
        return button;
    }

    public void showUserGuide() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("User Guide");
        alert.setHeaderText(null);
        alert.setContentText("In show map mode:\n" +
                "Use LEFT MOUSE BUTTON to add SKU to slot.\n" +
                "Use RIGHT MOUSE BUTTON to move map.\n" +
                "Use MOUSE WHEEL to change map size.\n" +
                "USE F1 to show help dialog.\n" +
                "Use LEFT MOUSE BUTTON on slot to add SKU to this slot.\n" +
                "Use LEFT MOUSE BUTTON + CTRL on slot to add SKU from chosen slot to order.\n" +
                "Use C to erase drawn path.\n" +
                "Use ESC to return to menu.");
        alert.showAndWait();
    }
}
