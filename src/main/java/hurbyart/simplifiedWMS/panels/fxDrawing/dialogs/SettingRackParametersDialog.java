package hurbyart.simplifiedWMS.panels.fxDrawing.dialogs;

import hurbyart.simplifiedWMS.panels.fxDrawing.MapCreationMainPanel;
import hurbyart.simplifiedWMS.panels.fxDrawing.UpperPaneMapCreation;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SettingRackParametersDialog {

    MapCreationMainPanel panelUsingMessages;
    static String[] savedParameters = new String[6];

    public SettingRackParametersDialog(MapCreationMainPanel panelUsingMessages) {
        this.panelUsingMessages = panelUsingMessages;
    }

    /**
     * Method for setting rack parameters dialog behavior
     *
     * @param alreadySet is needed to show userGuide if it is first dialog load
     * @return
     */
    public void act(boolean alreadySet) {
        Dialog<List<String>> dialog = new Dialog<>();
        dialog.setTitle("FIll in rack's width, length, height and slot width");
        dialog.setHeaderText(UpperPaneMapCreation.HELP_DIALOG_TEXT);
        if (alreadySet) dialog.setHeaderText(null);


        ButtonType setButtonType = new ButtonType("Set", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(setButtonType);
        if (alreadySet) dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(40, 150, 10, 10));

        TextField width = new TextField();
        TextField length = new TextField();
        TextField height = new TextField();
        TextField numberOfSlots = new TextField();

        String[] opt = {"1-slot", "2-slot"};
        ChoiceBox<String> choiceBoxSlotNumber = new ChoiceBox<>(FXCollections.observableArrayList(opt));
        choiceBoxSlotNumber.setMinWidth(40);


        String[] opt2 = {"horizontal", "vertical"};
        ChoiceBox<String> choiceBoxDirection = new ChoiceBox<>(FXCollections.observableArrayList(opt2));
        choiceBoxDirection.setMinWidth(80);

        if (savedParameters[0] == null) {
            width.setText("2000");
            length.setText("300");
            height.setText("300");
            numberOfSlots.setText("5");
            choiceBoxSlotNumber.setValue("2-slot");
            choiceBoxDirection.setValue("horizontal");
        } else {
            width.setText(savedParameters[0]);
            length.setText(savedParameters[1]);
            height.setText(savedParameters[2]);
            numberOfSlots.setText(savedParameters[3]);
            choiceBoxSlotNumber.setValue(savedParameters[4]);
            choiceBoxDirection.setValue(savedParameters[5]);
        }


        HBox hbox = new HBox(choiceBoxDirection, choiceBoxSlotNumber);
        hbox.setMinWidth(200);
        hbox.setAlignment(Pos.BASELINE_CENTER);
        hbox.setSpacing(10);

        grid.add(new Label("Rack width(cm):"), 0, 0);
        grid.add(width, 1, 0);
        grid.add(new Label("Rack length(cm):"), 0, 1);
        grid.add(length, 1, 1);
        grid.add(new Label("Rack height(cm):"), 0, 2);
        grid.add(height, 1, 2);
        grid.add(new Label("Number of slots/slots pair in rack:"), 0, 3);
        grid.add(numberOfSlots, 1, 3);
        grid.add(new Label("Rack type:"), 0, 4);
        grid.add(hbox, 1, 4);


        dialog.getDialogPane().setContent(grid);


        dialog.setResultConverter(dialogButton -> {
            List<String> parameters = Arrays.asList(width.getText(), length.getText(), height.getText(), numberOfSlots.getText(), choiceBoxSlotNumber.getValue(), choiceBoxDirection.getValue());
            if (dialogButton == setButtonType) {
                if (check(parameters)) {
                    savedParameters[0] = width.getText();
                    savedParameters[1] = length.getText();
                    savedParameters[2] = height.getText();
                    savedParameters[3] = numberOfSlots.getText();
                    savedParameters[4] = choiceBoxSlotNumber.getValue();
                    savedParameters[5] = choiceBoxDirection.getValue();
                    return parameters;
                }
            }
            return null;
        });
        Optional<List<String>> result = dialog.showAndWait();

        result.ifPresent(parameters -> {
            if (!check(parameters)) act(alreadySet);
        });
    }

    private boolean check(List<String> parameters) {
        for (int i = 0; i < 4; i++) {
            String param = parameters.get(i);
            if (param.isEmpty() || !param.matches("^[0-9.]*$")) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Fill contains not allowed symbols!\n" +
                        "Rack sizes are not set.\nYou can set it using F2 button.");
                alert.showAndWait();
                return false;
            }
        }

        double rackWidth = Double.parseDouble(parameters.get(0)) / 10;
        double rackLength = Double.parseDouble(parameters.get(1)) / 10;
        double rackHeight = Double.parseDouble(parameters.get(2)) / 10;
        int rackNumberOfSlots = Integer.parseInt(parameters.get(3));
        boolean twoSlotsRack = parameters.get(4).equals("2-slot");
        if (rackWidth < 0 || rackLength < 0 || rackHeight < 0 || rackNumberOfSlots < 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Fill contains not allowed numbers!\n" +
                    "Rack sizes are not set.\nYou can set it using F2 button.");
            return false;
        }
        boolean horizontal = parameters.get(5).equals("horizontal");
        panelUsingMessages.setRackParameters(rackWidth, rackLength, rackHeight, rackNumberOfSlots, twoSlotsRack, horizontal);

        return true;
    }

}
