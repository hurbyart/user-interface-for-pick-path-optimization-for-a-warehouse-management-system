package hurbyart.simplifiedWMS.panels.fxDrawing.dialogs;

import hurbyart.simplifiedWMS.panels.fxDrawing.MapCreationMainPanel;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.util.Optional;

public class CorrectingRackLocationDialog {

    MapCreationMainPanel panelUsingMessages;

    public CorrectingRackLocationDialog(MapCreationMainPanel panelUsingMessages) {
        this.panelUsingMessages = panelUsingMessages;
    }

    /**
     * Method for correcting rack location dialog behavior
     */
    public void act() {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Correct selected racks' positions");
        dialog.setHeaderText("Set coordinates or distance to align racks\nor make same gap between them:");


        ButtonType setButtonType = new ButtonType("Set", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(setButtonType);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(40, 150, 10, 10));

        TextField x = new TextField();
        x.setText("200");

        String[] opt = {"Align with X", "Align with Y", "Make same vertical gap", "Make same horizontal gap"};
        ChoiceBox choiceBox = new ChoiceBox(FXCollections.observableArrayList(opt));
        choiceBox.setValue("Align with X");

        grid.add(new Label("Distance(cm):"), 0, 0);
        grid.add(x, 1, 0);
        grid.add(new Label("Operation"), 0, 1);
        grid.add(choiceBox, 1, 1);
        dialog.getDialogPane().setContent(grid);


        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == setButtonType) {
                if (check(x.getText()))
                    panelUsingMessages.makeRackCorrection(new Pair<>(x.getText(), (String) choiceBox.getValue()));
            }
            return null;
        });
        Optional<Pair<String, String>> result = dialog.showAndWait();

        result.ifPresent(parameters -> {
            if (!check(x.getText())) {
                act();
                return;
            }
        });

    }

    public boolean check(String text) {
        if (text.isEmpty() || !text.matches("^[0-9.]*$")) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Fill contains not allowed symbols!");
            alert.showAndWait();
            return false;
        }
        return true;
    }
}
