package hurbyart.simplifiedWMS.panels.fxDrawing.dialogs;

import hurbyart.simplifiedWMS.db.AddDatabase;
import hurbyart.simplifiedWMS.db.DeleteDatabase;
import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.ShowMapPanel;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import hurbyart.simplifiedWMS.warehouseObjects.Slot;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AddingNewSKUToSlotDialog {

    ShowMapPanel panelUsingMessages;


    public AddingNewSKUToSlotDialog(ShowMapPanel panelUsingMessages) {
        this.panelUsingMessages = panelUsingMessages;
    }

    /**
     * Method for adding new SKU to slot dialog behavior
     *
     * @param slot
     */
    public void act(Slot slot) {
        Dialog<List<String>> dialog = new Dialog<>();
        dialog.setTitle("Add new SKU");
        dialog.setHeaderText("Adding new SKU to slot " + slot.getRackId() + "-" + slot.getId());


        ButtonType setButtonType = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(setButtonType);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(40, 150, 10, 10));

        TextField name = new TextField();
        TextField amountInOnePallet = new TextField();
        TextField description = new TextField();
        TextField width = new TextField();
        TextField length = new TextField();
        TextField height = new TextField();


        grid.add(new Label("Name:*"), 0, 0);
        grid.add(name, 1, 0);
        grid.add(new Label("Amount in one pallet:"), 0, 1);
        grid.add(amountInOnePallet, 1, 1);
        grid.add(new Label("Description :"), 0, 2);
        grid.add(description, 1, 2);
        grid.add(new Label("Width:"), 0, 3);
        grid.add(width, 1, 3);
        grid.add(new Label("Length:"), 0, 4);
        grid.add(length, 1, 4);
        grid.add(new Label("Height:"), 0, 5);
        grid.add(height, 1, 5);


        dialog.getDialogPane().setContent(grid);


        dialog.setResultConverter(dialogButton -> {
            List<String> parameters = Arrays.asList(name.getText(), amountInOnePallet.getText(), description.getText(), width.getText(), length.getText(), height.getText());
            if (dialogButton == setButtonType && check(parameters)) {

                int amountInOnePalletInt = 0;
                try {
                    amountInOnePalletInt = Integer.parseInt(amountInOnePallet.getText());
                } catch (NumberFormatException ex) {

                }
                Sku sku = new Sku(0, name.getText(), slot.getId(), amountInOnePalletInt, description.getText());
                if (width.getText() != null && !width.getText().equals("")) {
                    double widthDouble = Double.parseDouble(width.getText());
                    sku.setWidth(widthDouble);
                }
                if (length.getText() != null && !length.getText().equals("")) {
                    double lengthDouble = Double.parseDouble(length.getText());
                    sku.setLength(lengthDouble);
                }
                if (height.getText() != null && !height.getText().equals("")) {
                    double heightDouble = Double.parseDouble(height.getText());
                    sku.setHeight(heightDouble);
                }
                Sku alreadyInSku = GetDatabase.getOneSKUInSlot(slot.getId());
                if (alreadyInSku != null) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "There is already SKU " + alreadyInSku.getName() + " in slot.\n" +
                            "Are you that you want to add new SKU in this slot?\n" +
                            "Previous SKU will be deleted!");
                    alert.showAndWait();
                    if (alert.getResult() == ButtonType.OK) {
                        DeleteDatabase.deleteSKUbySlotId(slot.getId());
                        AddDatabase.addSKU(sku);
                    }
                } else {
                    AddDatabase.addSKU(sku);
                }
                return parameters;

            }
            return null;
        });
        Optional<List<String>> result = dialog.showAndWait();

        result.ifPresent(parameters -> {
            if (!check(parameters)) act(slot);
        });
    }

    private boolean check(List<String> parameters) {
        if (parameters.get(0).equals("")) {
            showAlert();
            return false;
        }
        return true;
    }

    private void showAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING, "SKU name is empty!\nSKU is not added!");
        alert.showAndWait();
    }
}
