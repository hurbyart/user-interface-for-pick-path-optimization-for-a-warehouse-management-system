package hurbyart.simplifiedWMS.panels.fxDrawing.dialogs;

import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.UpperPaneShowMap;
import hurbyart.simplifiedWMS.util.RangeParser;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Window;

import java.util.ArrayList;
import java.util.List;

public class CreatingOrderDialog {

    UpperPaneShowMap paneUsingDialog;
    List<Sku> skusInOrder = new ArrayList<>();

    public CreatingOrderDialog(UpperPaneShowMap paneUsingDialog) {
        this.paneUsingDialog = paneUsingDialog;
    }

    public List<Sku> getSkusInOrder() {
        return skusInOrder;
    }

    /**
     * Method for creating order dialog behavior
     */
    public void act() {
        Dialog<List<Sku>> dialog = new Dialog<>();
        dialog.setTitle("Add SKU to order");
        dialog.setHeaderText("In table are shown SKUs which are in order now.");
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> {
            for (Sku sku : skusInOrder) {
                paneUsingDialog.getShowMapPanel().changeColorOfSlot(sku.getSlotId(), Color.LIGHTYELLOW);
            }
            window.hide();
        });


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        TableView<Sku> table = new TableView<>();
        table.setEditable(true);
        table.setMinWidth(460);
        TableColumn idCol = new TableColumn("id");
        idCol.setMinWidth(100);
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn nameCol = new TableColumn("name");
        nameCol.setMinWidth(100);
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn slotIdCol = new TableColumn("slot_id");
        slotIdCol.setMinWidth(100);
        slotIdCol.setCellValueFactory(new PropertyValueFactory<>("slotId"));
        TableColumn descriptionCol = new TableColumn("description");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        descriptionCol.setMinWidth(160);

        table.getColumns().addAll(idCol, nameCol, slotIdCol, descriptionCol);
        table.getItems().addAll(skusInOrder);

        GridPane buttonsGrid = new GridPane();
        TextField field = new TextField();
        field.setPromptText("Product name or product ids(1, 3, 5-7)");
        field.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) addSkus(field, table);
        });

        table.setOnKeyPressed(e -> {
                    if (e.getCode() == KeyCode.DELETE) deleteSkuFromOrder(table);
                }
        );

        Button addButton = new Button("Add");
        addButton.setTooltip(new Tooltip("Use this button ot ENTER to add filled sku's to order"));
        addButton.setAlignment(Pos.CENTER);
        addButton.setMinWidth(140);

        Button deleteButton = new Button("Delete");
        addButton.setTooltip(new Tooltip("Use this button ot DELETE to delete selected sku from order"));
        deleteButton.setAlignment(Pos.CENTER);
        deleteButton.setMinWidth(140);

        Button finishOrderButton = new Button("Finish order");
        finishOrderButton.setTooltip(new Tooltip("Use this button to finish order and show pick path"));
        finishOrderButton.setAlignment(Pos.CENTER);
        finishOrderButton.setMinWidth(140);

        buttonsGrid.setPadding(new Insets(10, 10, 10, 10));

        grid.add(table, 0, 0);
        grid.add(new Label("Fill in SKU ID or name to add to order:"), 0, 1);
        grid.add(field, 0, 2);
        buttonsGrid.add(addButton, 0, 0);
        buttonsGrid.add(deleteButton, 1, 0);
        buttonsGrid.add(finishOrderButton, 2, 0);
        buttonsGrid.setVgap(5);
        buttonsGrid.setHgap(20);
        buttonsGrid.setAlignment(Pos.CENTER);

        GridPane checkboxGrid = new GridPane();

        CheckBox startPoint = new CheckBox("Use SP as starting point");
        startPoint.setSelected(true);
        startPoint.setPadding(new Insets(0, 10, 10, 10));
        CheckBox endPoint = new CheckBox("Use SP as end point");
        endPoint.setSelected(true);
        endPoint.setPadding(new Insets(0, 10, 10, 10));

        String[] opt = {"Dijkstra", "DijkstraPerm", "Johnson", "Floyd-Warshall"};
        ChoiceBox<String> choiceBox = new ChoiceBox<>(FXCollections.observableArrayList(opt));
        choiceBox.setValue("DijkstraPerm");

        checkboxGrid.add(startPoint, 0, 0);
        checkboxGrid.add(endPoint, 1, 0);
        checkboxGrid.add(choiceBox, 2, 0);
        checkboxGrid.setAlignment(Pos.CENTER);


        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);

        vBox.getChildren().addAll(grid, checkboxGrid);
        if (!GetDatabase.isSpConnected()) {
            startPoint.setSelected(false);
            endPoint.setSelected(false);
            startPoint.setDisable(true);
            endPoint.setDisable(true);
            Label spNotConnectedLabel = new Label("You cannot use SP as start/end, because it is not connected to any node.");
            spNotConnectedLabel.setPadding(new Insets(10, 10, 10, 10));
            spNotConnectedLabel.setTextFill(Color.GREY);
            vBox.getChildren().add(spNotConnectedLabel);
        }
        vBox.getChildren().add(buttonsGrid);
        dialog.getDialogPane().setContent(vBox);

        table.setOnMousePressed(e -> {
            for (int i = 0; i < skusInOrder.size(); i++) {
                Sku sku = skusInOrder.get(i);
                if (table.getSelectionModel().isSelected(i)) {
                    paneUsingDialog.getShowMapPanel().changeColorOfSlot(sku.getSlotId(), Color.PURPLE);
                } else {
                    paneUsingDialog.getShowMapPanel().changeColorOfSlot(sku.getSlotId(), Color.LIGHTYELLOW);
                }
            }
        });


        addButton.setOnAction(e -> {
            addSkus(field, table);
        });

        deleteButton.setOnAction(e -> {
            deleteSkuFromOrder(table);
        });

        finishOrderButton.setOnAction(e -> {
            if (skusInOrder.size() < 1) return;
            paneUsingDialog.getShowMapPanel().useAlgorithm(skusInOrder, startPoint.isSelected(),
                    endPoint.isSelected(), choiceBox.getValue());
            for (Sku sku : skusInOrder) {
                paneUsingDialog.getShowMapPanel().returnColorOfSlot(sku.getSlotId());
            }
            skusInOrder = new ArrayList<>();
            table.getItems().clear();
            window.hide();
        });
        dialog.showAndWait();

    }

    private void deleteSkuFromOrder(TableView<Sku> table) {
        Object selectedItem = table.getSelectionModel().getSelectedItem();
        if (selectedItem == null) return;
        Sku sku = (Sku) selectedItem;
        paneUsingDialog.getShowMapPanel().returnColorOfSlot(sku.getSlotId());
        table.getItems().remove(selectedItem);
        skusInOrder.remove(sku);
    }

    private void addSkus(TextField field, TableView table) {
        String text = field.getText();
        field.setText("");

        Sku sku = GetDatabase.getSkuByName(text);
        if (sku != null) {
            table.getItems().add(sku);
            skusInOrder.add(sku);
            paneUsingDialog.getShowMapPanel().changeColorOfSlot(sku.getSlotId(), Color.LIGHTYELLOW);
            return;
        }


        try {
            List<Integer> ids = RangeParser.getDistinctNumbers(text);
            for (Integer id : ids) {
                sku = GetDatabase.getSkuById(id);
                if (sku != null) {
                    table.getItems().add(sku);
                    skusInOrder.add(sku);
                    paneUsingDialog.getShowMapPanel().changeColorOfSlot(sku.getSlotId(), Color.LIGHTYELLOW);
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "SKU with id " + id + " was not found!");
                    alert.showAndWait();
                }
            }
            return;
        } catch (Exception ex) {

        }

        Alert alert = new Alert(Alert.AlertType.WARNING, "SKU with name/id " + text + " was not found!");
        alert.showAndWait();
    }

}
