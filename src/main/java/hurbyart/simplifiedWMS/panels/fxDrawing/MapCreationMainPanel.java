package hurbyart.simplifiedWMS.panels.fxDrawing;

import hurbyart.simplifiedWMS.db.AddDatabase;
import hurbyart.simplifiedWMS.db.DeleteDatabase;
import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.db.UpdateDatabase;
import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.Panels;
import hurbyart.simplifiedWMS.panels.fxDrawing.dialogs.CorrectingRackLocationDialog;
import hurbyart.simplifiedWMS.panels.fxDrawing.dialogs.SettingRackParametersDialog;
import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.StartingPointGraphics;
import hurbyart.simplifiedWMS.warehouseObjects.*;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.util.Pair;


import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


public class MapCreationMainPanel extends Panels {

    public static final int UPPER_MENU_HEIGHT = 40;
    public static final int WINDOW_WIDTH = 1080;
    public static final int WINDOW_HEIGHT = 720;


    private double rackWidth = 0;
    private double rackLength = 0;
    private double rackHeight = 0;
    private int rackNumberOfSlots = 0;
    private boolean twoSlotsRack = false;
    private boolean horizontalRack = false;
    private List<Rack> racks;
    private List<Rack> selectedRacks = new ArrayList<>();
    private List<NodeWMS> selectedNodes = new ArrayList<>();
    private List<Arc> arcsWithoutRack;
    private List<NodeWMS> nodesWithoutRack;

    JFrame frame;
    private double warehouseWidth;
    private double warehouseLength;
    CorrectingRackLocationDialog correctingRackLocationDialog;
    SettingRackParametersDialog settingRackParametersDialog;

    JFXPanel fxPanel;
    boolean selectionMode = false;
    boolean nodesAndArcsMode = false;
    boolean spMode = false;
    boolean dragMode = false;
    boolean removeMode = false;


    public MapCreationMainPanel(JFrame frame) {
        this.frame = frame;
        this.correctingRackLocationDialog = new CorrectingRackLocationDialog(this);
        this.settingRackParametersDialog = new SettingRackParametersDialog(this);
        double[] size;
        this.racks = GetDatabase.getAllRacks(1);

        this.nodesWithoutRack = GetDatabase.getAllNodesByRackId(0);
        this.arcsWithoutRack = GetDatabase.getAllArcsByRackId(0);
        size = GetDatabase.getWarehouseSize();
        Objects.requireNonNull(size);
        this.warehouseWidth = size[0];
        this.warehouseLength = size[1];
    }

    /**
     * Method for starting JavaFX panel
     */
    public void act() {
        frame.getContentPane().removeAll();
        frame.getContentPane().invalidate();
        fxPanel = new JFXPanel();
        Platform.runLater(() -> {
            Scene scene = createScene();
            fxPanel.setScene(scene);
        });
        frame.add(fxPanel);
        frame.setVisible(true);
        frame.getContentPane().revalidate();
    }


    /**
     * Method for setting parameters needed to create rack
     *
     * @param rackWidth
     * @param rackLength
     * @param rackHeight
     * @param rackNumberOfSlots
     * @param twoSlotsRack
     * @param horizontalRack
     */
    public void setRackParameters(double rackWidth, double rackLength, double rackHeight, int rackNumberOfSlots, boolean twoSlotsRack, boolean horizontalRack) {
        if (!horizontalRack) {
            double tmp = rackWidth;
            rackWidth = rackLength;
            rackLength = tmp;
        }
        this.rackWidth = rackWidth;
        this.rackLength = rackLength;
        this.rackHeight = rackHeight;
        this.rackNumberOfSlots = rackNumberOfSlots;
        this.twoSlotsRack = twoSlotsRack;
        this.horizontalRack = horizontalRack;
    }


    /**
     * Plan creation panel behavior method
     */
    private Scene createScene() {

        Group group = new Group();

        settingRackParametersDialog.act(false);
        PannablePane pPane = new PannablePane(warehouseWidth, warehouseLength);
        UpperPaneMapCreation upperPane = new UpperPaneMapCreation(WINDOW_WIDTH, UPPER_MENU_HEIGHT);


        group.getChildren().add(pPane);
        group.getChildren().add(upperPane);
        // center it
        pPane.setTranslateX(-(warehouseWidth - WINDOW_WIDTH) / 2);
        pPane.setTranslateY(-(warehouseLength - WINDOW_HEIGHT) / 2);

        // create scene which can be dragged and zoomed
        Scene scene = new Scene(group, WINDOW_WIDTH, WINDOW_HEIGHT);

        SceneGestures sceneGestures = new SceneGestures(pPane);
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, sceneGestures.getOnMousePressedEventHandler());
        scene.addEventFilter(MouseEvent.MOUSE_DRAGGED, sceneGestures.getOnMouseDraggedEventHandler());
        scene.addEventFilter(ScrollEvent.ANY, sceneGestures.getOnScrollEventHandler());


        StartingPointGraphics spg = new StartingPointGraphics();
        NodeWMS spNode = new NodeWMS(spg);
        nodesWithoutRack.add(spNode);

        Line[] lines = new Line[4];
        for (int i = 0; i < 4; i++) {
            lines[i] = new Line();
            lines[i].setStroke(Color.rgb(255, 0, 0, 0.5));
        }

        for (Rack rack : racks) {
            pPane.getChildren().addAll(rack.getGraphics().getRectangleAndLines());

            setDragListenersOnRack(rack, lines, pPane, upperPane);

            for (NodeWMS node : rack.getNodes()) {
                node.getGraphics().setTooltip();
            }
            for (Arc arc : rack.getArcs()) {
                arc.getGraphics().setTooltip();
                arc.getGraphics().getLine().setOnMouseClicked(e -> {
                    if (removeMode) deleteArc(pPane, arc);
                });
            }
        }


        for (Arc arc : arcsWithoutRack) {
            arc.getGraphics().getLine().setOnMouseClicked(e -> {
                if (removeMode) deleteArc(pPane, arc);
            });
        }

        pPane.setOnMouseMoved(e -> {
            double x = e.getX() / 10;
            double y = e.getY() / 10;
            String coordinates = String.format("Mouse cursor coordinates: %.2f m %.2f m", x, y);
            upperPane.setMouseCoordinates(coordinates);
        });


        pPane.setOnMousePressed(event -> {
            if (!event.isPrimaryButtonDown()) return;
            if (nodesAndArcsMode) {
                if (spMode) {
                    double x = event.getX();
                    double y = event.getY();
                    spg.getCircle().setCenterX(x);
                    spg.getCircle().setCenterY(y);
                    UpdateDatabase.updateSpInWarehouse(x, y);
                    for (Arc arc : arcsWithoutRack) arc.update();
                    return;
                }
                NodeWMS node = getNodeOnThisSpot(event.getX(), event.getY());
                if (!selectionMode) {
                    if (node == null && !removeMode) {
                        if (isPossibleToAddNode(event.getX(), event.getY())) {
                            NodeWMS newNode = new NodeWMS(0, event.getX(), event.getY(), null, 0);
                            AddDatabase.addNode(newNode, 0);
                            nodesWithoutRack.add(newNode);
                            newNode.getGraphics().setTooltip();
                            pPane.getChildren().add(newNode.getGraphics().getCircle());
                        }
                    } else {
                        if (removeMode && node != null) deleteNode(pPane, node);
                    }
                } else {
                    if (node == null) return;
                    if (!node.getGraphics().isSelected()) {
                        node.getGraphics().setSelected(true);
                        selectedNodes.add(node);
                    } else {
                        node.getGraphics().setSelected(false);
                        selectedNodes.remove(node);
                    }
                }
                return;
            }
            Rack rack = getRackOnThisSpot(event.getX(), event.getY());
            if (!selectionMode && !spMode) {
                if (rack == null) {
                    if (isPossibleToAddRack(event.getX(), event.getY()) && rackWidth > 0 && rackHeight > 0 && rackLength > 0) {
                        // Add a rack...
                        final Rack newRack = new Rack(0, rackWidth, rackLength, rackHeight, rackNumberOfSlots, twoSlotsRack, horizontalRack, event.getX(), event.getY()); // id 0?
                        // connecting new nodes with old if they are closer then 1/2 size iof slot
                        setDragListenersOnRack(newRack, lines, pPane, upperPane);
                        AddDatabase.addRack(newRack);
                        for (Rack r : racks) {
                            for (NodeWMS nodeOnMap : r.getNodes()) {
                                for (NodeWMS newNode : newRack.getNodes()) {
                                    if ((Math.abs(nodeOnMap.getX() - newNode.getX()) < newRack.getSlots().get(0).getSlotSizeY() / 2) &&
                                            (Math.abs(nodeOnMap.getY() - newNode.getY()) < newRack.getSlots().get(0).getSlotSizeY())) {
                                        Arc arc = new Arc(0, nodeOnMap, newNode, 0);
                                        arc.getGraphics().getLine().setOnMouseClicked(e -> {
                                            if (removeMode) deleteArc(pPane, arc);
                                        });
                                        AddDatabase.addArc(arc, 0);
                                        arcsWithoutRack.add(arc);
                                    }
                                }
                            }
                        }

                        pPane.getChildren().addAll(newRack.getGraphics().getRectangleAndLines());
                        racks.add(newRack);

                        if (nodesAndArcsMode) {
                            pPane.getChildren().addAll(newRack.getGraphics().getNodesAndLineGraphic());
                        }
                    }
                } else {
                    if (removeMode) {
                        if (checkIfAnySKUInRack(rack)) {
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Confirmation Dialog");
                            alert.setHeaderText("There are SKUs in rack, every sku will get slot_id = -1");
                            alert.setContentText("Do you agree?");

                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                deleteRack(pPane, rack);
                            }
                        } else {
                            deleteRack(pPane, rack);
                        }
                    }
                }
            } else {
                if (rack == null) return;
                else {
                    if (!rack.getGraphics().isSelected()) {
                        rack.getGraphics().setSelected(true);
                        selectedRacks.add(rack);
                    } else {
                        rack.getGraphics().setSelected(false);
                        selectedRacks.remove(rack);
                    }
                }
            }
        });

        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case ESCAPE:
                    PanelController.start(PanelMode.MENU);
                    break;
                case F1:
                    upperPane.showUserGuide();
                    break;
                case F2:
                    settingRackParametersDialog.act(true);
                    break;
                case F3:
                    correctingRackLocationDialog.act();
                    break;
                case F4:
                    connectSelectedNodesWithArcs(pPane);
                    break;
                case CONTROL:
                    selectionMode = true;
                    break;
                case SHIFT:
                    dragMode = true;
                    break;
                case N:
                    nodesAndArcsMode = !nodesAndArcsMode;
                    if (nodesAndArcsMode) upperPane.setNodeMode("Node mode");
                    else upperPane.setNodeMode("Rack mode");
                    addOrDeleteNodesFromPane(pPane);
                    break;
                case S:
                    spMode = true;
                    break;
                case DELETE:
                    if (!selectedNodes.isEmpty()) {
                        List<NodeWMS> copy = new ArrayList<>(selectedNodes);
                        for (NodeWMS n : copy) {
                            deleteNode(pPane, n);
                        }
                        selectedNodes = new ArrayList<>();
                    }
                    if (!selectedRacks.isEmpty()) {
                        List<Rack> copy = new ArrayList<>(selectedRacks);
                        for (Rack r : copy) {
                            deleteRack(pPane, r);
                        }
                        selectedRacks = new ArrayList<>();
                    }
                    removeMode = true;
                    break;
            }
        });

        scene.setOnKeyReleased(e -> {
            if (e.getCode() == KeyCode.CONTROL) {
                selectionMode = false;
            } else if (e.getCode() == KeyCode.S) {
                spMode = false;
            } else if (e.getCode() == KeyCode.SHIFT) {
                dragMode = false;
            } else if (e.getCode() == KeyCode.DELETE) {
                removeMode = false;
            }
        });
        scene.setFill(Color.TRANSPARENT);
        return scene;
    }

    private void setDragListenersOnRack(Rack rack, Line[] lines, PannablePane pPane, UpperPaneMapCreation upperPane) {
        double prevSavedX = rack.getX();
        double prevSavedY = rack.getY();
        rack.getGraphics().getRectangle().setOnMousePressed(

                e -> {
                    // prevent pannable ScrollPane from changing cursor on drag-detected
                    if (!dragMode || nodesAndArcsMode) return;
                    e.setDragDetect(false);
                    pPane.getChildren().addAll(lines);
                    Point2D offset =
                            new Point2D(e.getX() - rack.getGraphics().getRectangle().getX(), e.getY() - rack.getGraphics().getRectangle().getY());
                    rack.getGraphics().getRectangle().setUserData(offset);
                    upperPane.setNodeMode("Drag mode");
                    e.consume(); // prevents MouseEvent from reaching ScrollPane
                });
        rack.getGraphics().getRectangle().setOnMouseDragged(
                e -> {
                    // prevent pannable ScrollPane from changing cursor on drag-detected de || nodesAndArcsMode) return;
                    e.setDragDetect(false);
                    Point2D offset = (Point2D) rack.getGraphics().getRectangle().getUserData();
                    rack.moveXandY((int) e.getX() - (int) offset.getX(), (int) e.getY() - (int) offset.getY());
                    setLines(lines, rack);
                    for (Rack r : racks) {
                        if (rack.getX() == r.getX() || rack.getY() == r.getY()
                                || rack.getX() + rack.getRackSizeX() == r.getX() + r.getRackSizeX()
                                || rack.getY() + rack.getRackSizeY() == r.getY() + r.getRackSizeY()) {
                            r.getGraphics().setFill(Color.LIGHTGREEN);
                        } else r.getGraphics().setFill(Color.DODGERBLUE);
                    }
                    String coordinates = String.format("Rack coordinates: %.2f m %.2f m", rack.getX() / 10, rack.getY() / 10);
                    upperPane.setMouseCoordinates(coordinates);

                    e.consume(); // prevents MouseEvent from reaching ScrollPane
                });
        rack.getGraphics().getRectangle().setOnMouseReleased(e -> {
            if (!dragMode || nodesAndArcsMode) return;
            if (isPossibleToAddRack(rack.getX(), rack.getY())) {
                List<NodeWMS> nodes = new ArrayList<>(rack.getNodes());
                for (NodeWMS node : nodes) {
                    if (!(node.getX() >= 0 && node.getY() >= 0 && node.getX() <= warehouseWidth && node.getY() <= warehouseLength)) {
                        List<Arc> allArcs = new ArrayList<>(arcsWithoutRack);
                        allArcs.addAll(rack.getArcs());
                        for (Arc arc : allArcs) {
                            if (arc.getNode1().getId() == node.getId() || arc.getNode2().getId() == node.getId()) {
                                deleteArc(pPane, arc);
                            }
                        }
                        rack.getNodes().remove(node);
                        deleteNode(pPane, node);
                    } else {
                        List<Arc> allArcs = new ArrayList<>(arcsWithoutRack);
                        allArcs.addAll(rack.getArcs());
                        for (Arc arc : allArcs) {
                            if (arc.getNode1().getId() == node.getId()) {
                                arc.getNode1().setX(node.getX());
                                arc.getNode1().setY(node.getY());
                                arc.update();
                            }
                            if (arc.getNode2().getId() == node.getId()) {
                                arc.getNode2().setX(node.getX());
                                arc.getNode2().setY(node.getY());
                                arc.update();
                            }
                        }
                        upperPane.setNodeMode("Rack mode");
                    }
                }
                UpdateDatabase.updateRack(rack);
            } else {
                rack.moveXandY(prevSavedX, prevSavedY);
            }
            for (Rack r : racks) {
                r.getGraphics().setFill(Color.DODGERBLUE);
            }
            pPane.getChildren().removeAll(lines);
            for (int i = 0; i < 4; i++) {
                lines[i] = new Line();
                lines[i].setStroke(Color.rgb(255, 0, 0, 0.5));
            }
        });
    }

    private void setLines(Line[] lines, Rack rack) {
        lines[0].setStartX(rack.getX());
        lines[0].setEndX(rack.getX());
        lines[0].setStartY(0);
        lines[0].setEndY(warehouseLength);
        lines[1].setStartX(rack.getX() + rack.getRackSizeX());
        lines[1].setEndX(rack.getX() + rack.getRackSizeX());
        lines[1].setStartY(0);
        lines[1].setEndY(warehouseLength);
        lines[2].setStartX(0);
        lines[2].setEndX(warehouseWidth);
        lines[2].setStartY(rack.getY());
        lines[2].setEndY(rack.getY());
        lines[3].setStartX(0);
        lines[3].setEndX(warehouseWidth);
        lines[3].setStartY(rack.getY() + rack.getRackSizeY());
        lines[3].setEndY(rack.getY() + rack.getRackSizeY());
    }

    private List<Shape> getAllFreeNodesCirclesAndArcsLines() {
        List<Shape> ret = new ArrayList<>();
        for (Arc arc : arcsWithoutRack) {
            ret.add(arc.getGraphics().getLine());
            arc.getGraphics().setTooltip();
        }
        for (NodeWMS node : nodesWithoutRack) {
            ret.add(node.getGraphics().getCircle());
            node.getGraphics().setTooltip();
        }
        return ret;
    }

    private void addOrDeleteNodesFromPane(PannablePane pPane) {
        if (nodesAndArcsMode) {
            for (Rack rack : racks) {
                pPane.getChildren().addAll(rack.getGraphics().getNodesAndLineGraphic());
            }
            pPane.getChildren().addAll(getAllFreeNodesCirclesAndArcsLines());
        } else {
            for (Rack rack : racks) {
                pPane.getChildren().removeAll(rack.getGraphics().getNodesAndLineGraphic());
            }
            pPane.getChildren().removeAll(getAllFreeNodesCirclesAndArcsLines());
        }
    }

    private void connectSelectedNodesWithArcs(PannablePane pPane) {
        for (int i = 0; i < selectedNodes.size(); i++) {
            if (i != selectedNodes.size() - 1) {
                NodeWMS node1 = selectedNodes.get(i);
                NodeWMS node2 = selectedNodes.get(i + 1);
                Arc arc = new Arc(0, node1, node2, 0);
                arc.getGraphics().getLine().setOnMouseClicked(e -> {
                    if (removeMode) deleteArc(pPane, arc);
                });
                AddDatabase.addArc(arc, 0);
                arcsWithoutRack.add(arc);
                arc.getGraphics().setTooltip();
                // manipulations with nodes done to draw nodes over arc
                pPane.getChildren().remove(node1.getGraphics().getCircle());
                pPane.getChildren().remove(node2.getGraphics().getCircle());
                pPane.getChildren().add(arc.getGraphics().getLine());
                pPane.getChildren().add(node1.getGraphics().getCircle());
                pPane.getChildren().add(node2.getGraphics().getCircle());
            }
            selectedNodes.get(i).getGraphics().setSelected(false);
        }
        selectedNodes = new ArrayList<>();
    }


    private void deleteNode(PannablePane pPane, NodeWMS node) {
        if (node.getId() == -1) return;
        pPane.getChildren().remove(node.getGraphics().getCircle());
        for (Rack rack : racks) {
            List<Arc> arcs = new ArrayList<>(rack.getArcs());
            for (Arc arc : arcs) {
                if (arc.getNode1().equals(node) || arc.getNode2().equals(node)) {
                    deleteArc(pPane, arc);
                    rack.getArcs().remove(arc);
                }
            }
        }

        List<Arc> arcs = new ArrayList<>(arcsWithoutRack);
        for (Arc arc : arcs) {
            if (arc.getNode1().equals(node) || arc.getNode2().equals(node)) {
                deleteArc(pPane, arc);
                arcsWithoutRack.remove(arc);
            }
        }
        for (Rack rack : racks) {
            if (rack.getId() == node.getRackId()) rack.getNodes().remove(node);
        }
        DeleteDatabase.deleteNodeByID(node.getId());
        nodesWithoutRack.remove(node);
        selectedNodes.remove(node);
    }


    private void deleteArc(PannablePane pPane, Arc arc) {
        pPane.getChildren().remove(arc.getGraphics().getLine());
        DeleteDatabase.deleteArcByID(arc.getId());
    }

    private void deleteRack(PannablePane pPane, Rack rack) {
        pPane.getChildren().removeAll(rack.getGraphics().getRectangleAndLines());
        pPane.getChildren().removeAll(rack.getGraphics().getNodesAndLineGraphic());

        DeleteDatabase.deleteRackByID(rack.getId());
        DeleteDatabase.deleteSlotByRackID(rack.getId());
        List<NodeWMS> nodes = new ArrayList<>(rack.getNodes());
        for (NodeWMS node : nodes) {
            deleteNode(pPane, node);
        }
        DeleteDatabase.deleteNodeByRackID(rack.getId());
        List<Arc> arcs = new ArrayList<>(rack.getArcs());
        for (Arc arc : arcs) {
            deleteArc(pPane, arc);
        }
        //DeleteDatabase.de
        DeleteDatabase.deleteArcByRackID(rack.getId());

        selectedRacks.remove(rack);
        racks.remove(rack);
        DeleteDatabase.deleteRackByID(rack.getId());
    }

    private boolean checkIfAnySKUInRack(Rack rack) {
        for (Slot slot : rack.getSlots()) {
            List<Sku> skus = GetDatabase.getAllSKUInSlot(slot.getId());

            if (skus != null && !skus.isEmpty()) return true;
        }
        return false;
    }

    /**
     * Method for changing rack position using racks selection and F4 dialog
     *
     * @param options
     */
    public void makeRackCorrection(Pair<String, String> options) {
        String operation = options.getValue();
        String numberStr = options.getKey();
        if (operation.isEmpty() || numberStr.isEmpty()) return;
        double number = Double.parseDouble(numberStr) / 10;
        List<Rack> sortedSelected;
        double accumulator;
        switch (operation) {
            case "Align with X":
                for (Rack rack : selectedRacks) {
                    rack.moveX(number);
                }
                break;
            case "Align with Y":
                for (Rack rack : selectedRacks) {
                    rack.moveY(number);
                }
                break;
            case ("Make same vertical gap"):
                sortedSelected = selectedRacks.stream()
                        .sorted(Comparator.comparingDouble(Rack::getY))
                        .collect(Collectors.toList());
                accumulator = sortedSelected.get(0).getY() + sortedSelected.get(0).getRackSizeY() + number;
                for (int i = 1; i < sortedSelected.size(); i++) {
                    Rack rack = sortedSelected.get(i);
                    rack.moveY(accumulator);
                    accumulator += rack.getRackSizeY() + number;
                }
                break;
            case ("Make same horizontal gap"):
                sortedSelected = selectedRacks.stream()
                        .sorted(Comparator.comparingDouble(Rack::getX))
                        .collect(Collectors.toList());
                accumulator = sortedSelected.get(0).getX() + sortedSelected.get(0).getRackSizeX() + number;
                for (int i = 1; i < sortedSelected.size(); i++) {
                    Rack rack = sortedSelected.get(i);
                    rack.moveX(accumulator);
                    accumulator += rack.getRackSizeX() + number;
                }
                break;
        }
        for (Rack rack : selectedRacks) {
            for (NodeWMS node : rack.getNodes()) {
                List<Arc> allArcs = new ArrayList<>(arcsWithoutRack);
                allArcs.addAll(rack.getArcs());
                for (Arc arc : allArcs) {
                    if (arc.getNode1().getId() == node.getId()) {
                        arc.getNode1().setX(node.getX());
                        arc.getNode1().setY(node.getY());
                        arc.update();
                    }
                    if (arc.getNode2().getId() == node.getId()) {
                        arc.getNode2().setX(node.getX());
                        arc.getNode2().setY(node.getY());
                        arc.update();
                    }
                }
            }
        }
        for (Arc arc : arcsWithoutRack) {
            arc.update();
        }
        for (Rack rack : selectedRacks) {
            rack.getGraphics().setSelected(false);
            UpdateDatabase.updateRack(rack);
        }
        selectionMode = false;
        selectedRacks = new ArrayList<>();
    }


    private Rack getRackOnThisSpot(double x, double y) {
        for (Rack rack : racks) {
            if (rack.getGraphics().isInside(x, y)) {
                return rack;
            }
        }
        return null;
    }

    private NodeWMS getNodeOnThisSpot(double x, double y) {
        for (NodeWMS node : nodesWithoutRack) {
            if (node.getGraphics().isInside(x, y)) return node;
        }
        for (Rack rack : racks) {
            for (NodeWMS node : rack.getNodes()) {
                if (node.getGraphics().isInside(x, y)) return node;
            }
        }
        return null;
    }


    private boolean isPossibleToAddRack(double x, double y) {
        if (x < 0 || y < 0) return false;
        if (x + rackWidth > warehouseWidth) return false;
        if (y + rackLength > warehouseLength) return false;
        for (Rack rack : racks) {
            if (!(rack.getX() == x && rack.getY() == y)) {
                if (rack.getGraphics().isInside(x, y)) return false;
                if (rack.getGraphics().isInside(x + rackWidth, y)) return false;
                if (rack.getGraphics().isInside(x, y + rackLength)) return false;
                if (rack.getGraphics().isInside(x + rackWidth, y + rackLength)) return false;
                if (rack.getX() > x && rack.getX() + rack.getRackSizeX() < x + rackWidth &&
                        ((y < rack.getY() + rack.getRackSizeY() && y > rack.getY()) || (y + rackLength < rack.getY() + rack.getRackSizeY() && y + rackLength > rack.getY())))
                    return false;

                if (rack.getY() > y && rack.getY() + rack.getRackSizeY() < y + rackLength &&
                        ((x < rack.getX() + rack.getRackSizeX() && x > rack.getX()) || (x + rackWidth < rack.getX() + rack.getRackSizeX() && x + rackWidth > rack.getX())))
                    return false;
            }
        }
        return true;
    }

    private boolean isPossibleToAddNode(double x, double y) {
        if (x > warehouseWidth) return false;
        if (y > warehouseLength) return false;
        for (Rack rack : racks) {
            if (rack.getGraphics().isInside(x, y)) return false;
        }
        return true;
    }


}
