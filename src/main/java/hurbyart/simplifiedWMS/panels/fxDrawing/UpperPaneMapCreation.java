package hurbyart.simplifiedWMS.panels.fxDrawing;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

public class UpperPaneMapCreation extends Pane {

    private static final String HELP_IMAGE = "help_button_icon.png";
    public static final String HELP_DIALOG_TEXT = "In creation map mode:\n" +
            "Use LEFT MOUSE BUTTON to add or delete rack.\n" +
            "Use RIGHT MOUSE BUTTON to move map.\n" +
            "Use MOUSE WHEEL to change map size.\n" +
            "Use CTRL + CLICK to select rack or node.\n" +
            "Use F1 to show help dialog.\n" +
            "Use F2 to call change rack size dialog.\n" +
            "Use F3 to align selected racks.\n" +
            "Use F4 to connect selected nodes.\n" +
            "Use DELETE + CLICK to delete rack, arc or node.\n" +
            "Use SHIFT + MOUSE DRAG to drag rack.\n" +
            "Use N to turn on/of node mode.\n" +
            "Hold S in node mode to change starting point location.\n" +
            "Use ESC to return to menu.";


    private Label nodeMode;
    private Label mouseCoordinates;
    private Button helpButton;

    public UpperPaneMapCreation(double width, double height) {

        setStyle("-fx-background-color: #4085a2; -fx-border-color: orange;");
        setPrefSize(width, height);

        Font font = new Font(22);

        this.nodeMode = createNodeMode(font);
        this.mouseCoordinates = createMouseCoordinatesLabel(font);
        this.helpButton = createHelpButton();

        this.getChildren().add(nodeMode);
        this.getChildren().add(mouseCoordinates);
        this.getChildren().add(helpButton);
    }

    private Label createMouseCoordinatesLabel(Font font) {
        Label mouseCoordinates = new Label("Mouse Location Monitor");
        mouseCoordinates.setTranslateX(10);
        mouseCoordinates.setTranslateY(5);
        mouseCoordinates.setFont(font);
        return mouseCoordinates;
    }

    private Label createNodeMode(Font font) {
        Label nodeMode = new Label("Rack mode");
        nodeMode.setFont(font);
        nodeMode.setTranslateX(900);
        nodeMode.setTranslateY(5);
        return nodeMode;
    }


    private Button createHelpButton() {
        Image img = new Image(HELP_IMAGE);
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setFitWidth(20);
        view.setPreserveRatio(true);

        Button button = new Button();
        button.setTranslateX(1040);
        button.setTranslateY(5);
        button.setGraphic(view);

        button.setOnAction(e -> {
            showUserGuide();
        });
        return button;
    }

    public void showUserGuide() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("User Guide");
        alert.setHeaderText(null);
        alert.setContentText(HELP_DIALOG_TEXT);

        alert.showAndWait();
    }

    public void setMouseCoordinates(String mouseCoordinatesStr) {
        this.mouseCoordinates.setText(mouseCoordinatesStr);
    }

    public void setNodeMode(String nodeModeStr) {
        this.nodeMode.setText(nodeModeStr);
    }

}
