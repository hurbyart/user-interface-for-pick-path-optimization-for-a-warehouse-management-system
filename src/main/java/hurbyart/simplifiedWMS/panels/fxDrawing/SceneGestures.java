package hurbyart.simplifiedWMS.panels.fxDrawing;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

public class SceneGestures {

    private static final double MAX_SCALE = 100.0d;
    private static final double MIN_SCALE = .01d;

    private DragContext sceneDragContext = new DragContext();

    PannablePane pPane;

    public SceneGestures(PannablePane pPane) {
        this.pPane = pPane;
    }

    public EventHandler<MouseEvent> getOnMousePressedEventHandler() {
        return onMousePressedEventHandler;
    }

    public EventHandler<MouseEvent> getOnMouseDraggedEventHandler() {
        return onMouseDraggedEventHandler;
    }

    public EventHandler<ScrollEvent> getOnScrollEventHandler() {
        return onScrollEventHandler;
    }

    private EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {
        //panning
        @Override
        public void handle(MouseEvent event) {
            if (!event.isSecondaryButtonDown()) {
                return;
            }
            sceneDragContext.mouseAnchorX = event.getSceneX();
            sceneDragContext.mouseAnchorY = event.getSceneY();
            sceneDragContext.translateAnchorX = pPane.getTranslateX();
            sceneDragContext.translateAnchorY = pPane.getTranslateY();
        }
    };

    private final EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        // panning
        @Override
        public void handle(MouseEvent event) {
            if (!event.isSecondaryButtonDown()) {
                return;
            }
            pPane.setTranslateX(sceneDragContext.translateAnchorX + event.getSceneX() - sceneDragContext.mouseAnchorX);
            pPane.setTranslateY(sceneDragContext.translateAnchorY + event.getSceneY() - sceneDragContext.mouseAnchorY);
            event.consume();
        }
    };

    private final EventHandler<ScrollEvent> onScrollEventHandler = new EventHandler<ScrollEvent>() {
        // zooming
        @Override
        public void handle(ScrollEvent event) {
            double delta = 1.2;

            double scale = pPane.getScale();
            double oldScale = scale;

            if (event.getDeltaY() < 0) {
                scale /= delta;
            } else {
                scale *= delta;
            }
            scale = clamp(scale, MIN_SCALE, MAX_SCALE);
            double f = (scale / oldScale) - 1;
            double dx = (event.getSceneX() - (pPane.getBoundsInParent().getWidth() / 2 + pPane.getBoundsInParent().getMinX()));
            double dy = (event.getSceneY() - (pPane.getBoundsInParent().getHeight() / 2 + pPane.getBoundsInParent().getMinY()));

            pPane.setScale(scale);
            // pivot value must be untransformed, i. e. without scaling
            pPane.setPivot(f * dx, f * dy);

            event.consume();
        }
    };

    public static double clamp(double value, double min, double max) {
        if (Double.compare(value, min) < 0) {
            return min;
        }
        if (Double.compare(value, max) > 0) {
            return max;
        }
        return value;
    }
}
