package hurbyart.simplifiedWMS.panels;

public enum PanelMode {
    MENU,
    MAP_CREATION,
    MAP_CREATION2,
    PRODUCTS_LIST,
    SHOW_MAP
}
