package hurbyart.simplifiedWMS.panels.swingDrawing;

import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.Panels;

import java.awt.*;
import javax.swing.*;


public class MenuPanel extends Panels {

    JFrame frame;

    public MenuPanel(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Menu panel behavior method
     */
    public void act() {
        frame.getContentPane().removeAll();
        frame.getContentPane().invalidate();

        JPanel menuPanel = new JPanel();

        JLabel programName = new JLabel();
        programName.setText("Pick path optimizer for WMS");
        programName.setBounds(260, 20, 720, 79);
        programName.setForeground(Color.WHITE);
        programName.setFont(new Font("hz", Font.PLAIN, 40));


        JButton productListButton = createButton("Product list", 365, 120, 360, 100, 40, e -> PanelController.start(PanelMode.PRODUCTS_LIST));
        JButton mapCreatingButton = createButton("Map creating", 365, 260, 360, 100, 40, e -> {
            PanelController.start(PanelMode.MAP_CREATION);
        });
        JButton showMapButton = createButton("Show map", 365, 400, 360, 100, 40, e -> PanelController.start(PanelMode.SHOW_MAP));
        JButton b4 = createButton("Exit", 365, 540, 360, 100, 40, e -> System.exit(0));


        menuPanel.add(programName);
        menuPanel.add(productListButton);
        menuPanel.add(mapCreatingButton);
        menuPanel.add(showMapButton);
        menuPanel.add(b4);
        menuPanel.setLayout(null);
        menuPanel.setSize(1080, 720);
        menuPanel.setBackground(new Color(64, 64, 64));


        frame.getContentPane().add(menuPanel);
        frame.getContentPane().revalidate();

    }
}
