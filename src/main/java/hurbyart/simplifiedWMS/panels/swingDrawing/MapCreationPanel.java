package hurbyart.simplifiedWMS.panels.swingDrawing;

import hurbyart.simplifiedWMS.db.DeleteDatabase;
import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.db.UpdateDatabase;
import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.Panels;

import javax.swing.*;
import java.awt.*;


import static javax.swing.JOptionPane.showMessageDialog;


public class MapCreationPanel extends Panels {


    JFrame frame;
    private static Double warehouseWidth = 0.0;
    private static Double warehouseLength = 0.0;

    public MapCreationPanel(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Map creation panel behavior method
     */
    public void act() {
        frame.getContentPane().removeAll();
        frame.getContentPane().invalidate();

        JPanel createMapPanelSetSize = new JPanel();

        JLabel mapCreationName = createLabel("Map creating", 400, 20, 720, 79);
        JLabel creatingInfo = createLabel("ENTER WAREHOUSE SIZE", 285, 100, 720, 79);
        JLabel widthInfo = createLabel("width: (m)", 475, 200, 220, 79);
        JLabel lengthInfo = createLabel("length: (m)", 465, 400, 250, 79);


        JTextField widthField = createField(290, 270, 500, 100);
        JTextField lengthField = createField(290, 470, 500, 100);
        widthField.setHorizontalAlignment(JTextField.CENTER);
        lengthField.setHorizontalAlignment(JTextField.CENTER);


        widthField.setText("90");
        lengthField.setText("60");

        boolean alreadyDrawnWarehouse = GetDatabase.isWarehousePlanExist() > 0;

        int x = 370;

        if (alreadyDrawnWarehouse) {
            JButton oldWarehouseButton = createButton("LOAD OLD", 550, 590, 350, 90, 25, e -> {
                PanelController.start(PanelMode.MAP_CREATION2);
            });
            createMapPanelSetSize.add(oldWarehouseButton);
            x = 150;
        }

        JButton newWarehouseButton = createButton("CREATE NEW", x, 590, 350, 90, 25, e -> {
            if (check(widthField.getText(), lengthField.getText())) {
                DeleteDatabase.deleteAllData();
                UpdateDatabase.updateWarehouseSize(warehouseWidth.toString(), warehouseLength.toString());
                PanelController.start(PanelMode.MAP_CREATION2);
            } else act();

        });


        createMapPanelSetSize.add(widthField);
        createMapPanelSetSize.add(lengthField);
        createMapPanelSetSize.add(newWarehouseButton);
        createMapPanelSetSize.add(addReturnButton(PanelMode.MENU));
        createMapPanelSetSize.add(mapCreationName);
        createMapPanelSetSize.add(creatingInfo);
        createMapPanelSetSize.add(widthInfo);
        createMapPanelSetSize.add(lengthInfo);
        createMapPanelSetSize.setLayout(null);
        createMapPanelSetSize.setSize(1080, 720);
        createMapPanelSetSize.setBackground(new Color(64, 64, 64));


        frame.getContentPane().add(createMapPanelSetSize);
        frame.getContentPane().revalidate();
    }


    private boolean check(String width, String length) {
        if (length.isEmpty() || width.isEmpty()) {
            showMessageDialog(this, "One ot both fields are empty!\nSize wasn't set!");
            return false;
        }
        if (!length.matches("^[0-9.]*$")) {
            showMessageDialog(this, "Width field has unacceptable symbols!\nMap wasn't created!");
            return false;
        }
        if (!width.matches("^[0-9.]*$")) {
            showMessageDialog(this, "Length field has unacceptable symbols!\nMap wasn't created!");
            return false;
        }
        warehouseWidth = Double.parseDouble(width) * 10;
        warehouseLength = Double.parseDouble(length) * 10;
        return true;
    }


}
