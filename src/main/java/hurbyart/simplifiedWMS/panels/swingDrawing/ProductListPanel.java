package hurbyart.simplifiedWMS.panels.swingDrawing;

import hurbyart.simplifiedWMS.db.AddDatabase;
import hurbyart.simplifiedWMS.db.DeleteDatabase;
import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.db.UpdateDatabase;
import hurbyart.simplifiedWMS.panels.PanelMode;
import hurbyart.simplifiedWMS.panels.Panels;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;


public class ProductListPanel extends Panels {

    public static final int FIELD_AMOUNT = 7;
    private static final String HELP_IMAGE = "help_button_icon.png";
    JFrame frame;

    public ProductListPanel(JFrame frame) {
        this.frame = frame;

    }

    /**
     * Product list panel behavior method
     */
    public void act() {
        frame.getContentPane().removeAll();
        frame.getContentPane().invalidate();

        JPanel productListPanel = new JPanel();
        JLabel productListName = new JLabel();


        productListName.setText("Products list");
        productListName.setBounds(430, 20, 720, 79);
        productListName.setForeground(Color.WHITE);
        Color color = new Color(64, 64, 64);
        productListName.setFont(new Font("font", Font.PLAIN, 40));

        Object[][] data = GetDatabase.getAllProducts();


        String[] columnNames = {"id", "name", "slot_id", "description", "width", "length", "height", "amount_in_one_pallet"};
        JTable table = new JTable(data, columnNames);

        table.setDragEnabled(false);
        table.setDefaultEditor(Object.class, null);
        table.setForeground(Color.WHITE);
        table.setBackground(color);
        table.setRowHeight(30);
        table.getColumnModel().getSelectionModel().addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int row = table.getSelectedRow();
                int column = table.getSelectedColumn();
                String toShow = table.getValueAt(row, column).toString();
                if (column != 0) {
                    String newValue = showInputDialog(this, data[row][1] + " " + columnNames[column] + "\nOld value: " + toShow + "\nNew value:");
                    Sku sku = GetDatabase.getSkuById(Integer.parseInt(table.getValueAt(row, 0).toString()));
                    Object obj = changeSku(sku, newValue, column);
                    if (obj == null) return;
                    data[row][column] = obj;
                    UpdateDatabase.updateSku(sku);
                    table.repaint();
                }
            }

        });


        table.addKeyListener(
                new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                    }

                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                            int[] selected = table.getSelectedRows();
                            for (int i : selected) {
                                DeleteDatabase.deleteSKU((int) data[i][0]);
                                act();
                            }
                        }

                    }

                    @Override
                    public void keyReleased(KeyEvent e) {
                    }
                }
        );

        int fieldWidth = 900 / columnNames.length;
        int filedHeight = 25;
        JTextField[] fields = new JTextField[FIELD_AMOUNT];
        fields[0] = new HintTextField("name");
        fields[1] = new HintTextField("slot_id");
        fields[2] = new HintTextField("description");
        fields[3] = new HintTextField("width");
        fields[4] = new HintTextField("length");
        fields[5] = new HintTextField("height");
        fields[6] = new HintTextField("amount_in_one_pallet");
        for (int i = 0; i < FIELD_AMOUNT; i++) {
            if (i == FIELD_AMOUNT - 1) {
                fields[i].setBounds(90 + (i + 1) * fieldWidth, 650, fieldWidth + 4, filedHeight);
            } else {
                fields[i].setBounds(90 + (i + 1) * fieldWidth, 650, fieldWidth, filedHeight);
            }
            fields[i].setHorizontalAlignment(JTextField.CENTER);
            fields[i].setFont(new Font("f", Font.BOLD, 10));
            fields[i].setBackground(Color.white);
            productListPanel.add(fields[i]);
        }


        JScrollPane sp = new JScrollPane(table);
        sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sp.setBounds(90, 100, 915, 550);
        sp.getViewport().setBackground(color);

        JButton addProductButton = createButton("Add", 90, 650, fieldWidth, filedHeight, 10, e -> {
            if (check(getAllFieldsText(fields))) {
                addSKUtoDB(getAllFieldsText(fields));
                act();
            }
        });

        JButton helpButton = new JButton("?");
        helpButton.setBounds(1030, 10, 40, 40);
        helpButton.setFont(new Font("f", Font.PLAIN, 12));

        productListPanel.setFocusable(true);
        productListPanel.requestFocusInWindow();
        productListPanel.addKeyListener(
                new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                    }

                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_G) {
                            showHelp();
                        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                            System.out.println("here");
                        }
                    }

                    @Override
                    public void keyReleased(KeyEvent e) {
                    }
                }
        );

        helpButton.addActionListener(e -> showHelp());
        productListPanel.add(sp);
        productListPanel.add(helpButton);
        productListPanel.add(addProductButton);
        productListPanel.add(productListName);
        productListPanel.add(addReturnButton(PanelMode.MENU));
        productListPanel.setLayout(null);
        productListPanel.setSize(1080, 720);
        productListPanel.setBackground(color);

        frame.getContentPane().add(productListPanel);
        frame.getContentPane().revalidate();

    }

    private void showHelp() {
        showMessageDialog(this, "ADDING sku is possible using bottom fields.\n" +
                "EDITING is possible using click on cell.\nREMOVING sku is possible using DELETE button on selected row.");
    }

    private Object changeSku(Sku sku, String newValue, int column) {
        if (column == 1) {
            sku.setName(newValue);
            return newValue;
        }
        if (column == 2) {
            int slotId;
            try {
                slotId = Integer.parseInt(newValue);
                sku.setSlotId(slotId);
                return slotId;
            } catch (Exception ex) {
                return null;
            }
        }
        if (column == 3) {
            sku.setDescription(newValue);
            return newValue;
        }
        if (column == 4) {
            double width;
            try {
                width = Double.parseDouble(newValue);
                sku.setWidth(width);
                return width;
            } catch (Exception ex) {
                return null;
            }
        }
        if (column == 5) {
            double length;
            try {
                length = Double.parseDouble(newValue);
                sku.setLength(length);
                return length;
            } catch (Exception ex) {
                return null;
            }
        }
        if (column == 6) {
            double height;
            try {
                height = Double.parseDouble(newValue);
                sku.setHeight(height);
                return height;
            } catch (Exception ex) {
                return null;
            }
        }
        if (column == 7) {
            int amount;
            try {
                amount = Integer.parseInt(newValue);
                sku.setAmountInOnePallet(amount);
                return amount;
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }


    private String[] getAllFieldsText(JTextField[] fields) {
        String[] allTextFields = new String[FIELD_AMOUNT];
        for (int i = 0; i < FIELD_AMOUNT; i++) {
            allTextFields[i] = fields[i].getText();
        }
        return allTextFields;
    }

    private void addSKUtoDB(String[] allTextFields) {
        int slotId = -1;
        try {
            slotId = Integer.parseInt(allTextFields[1]);
        } catch (NumberFormatException ex) {

        }
        int amountInOnePalletInt = 0;
        try {
            amountInOnePalletInt = Integer.parseInt(allTextFields[6]);
        } catch (NumberFormatException ex) {

        }
        Sku sku = new Sku(0, allTextFields[0], slotId, amountInOnePalletInt, allTextFields[2]);
        if (!allTextFields[3].isEmpty()) sku.setWidth(Double.parseDouble(allTextFields[3]));
        if (!allTextFields[4].isEmpty()) sku.setLength(Double.parseDouble(allTextFields[4]));
        if (!allTextFields[5].isEmpty()) sku.setHeight(Double.parseDouble(allTextFields[5]));
        AddDatabase.addSKU(sku);
    }

    private boolean check(String[] allTextFields) {
        return !allTextFields[0].isEmpty() && intCheck(allTextFields[1]) && doubleCheck(allTextFields[3])
                && doubleCheck(allTextFields[4]) && doubleCheck(allTextFields[5]) && intCheck(allTextFields[6]);
    }

    private boolean intCheck(String str) {
        if (str.isEmpty()) return true;
        return str.matches("^[0-9.]*$");
    }

    private boolean doubleCheck(String str) {
        if (str.isEmpty()) return true;
        return str.matches("^[0-9.]*$");
    }
}
