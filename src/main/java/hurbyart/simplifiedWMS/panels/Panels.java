package hurbyart.simplifiedWMS.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public abstract class Panels extends JFrame {

    public abstract void act();

    /**
     * Method to simplify swing button creation
     *
     * @param text
     * @param x
     * @param y
     * @param width
     * @param height
     * @param fontSize
     * @param listener
     * @return button
     */
    public JButton createButton(String text, int x, int y, int width, int height, int fontSize, ActionListener listener) {
        JButton button = new JButton(text);
        button.setBounds(x, y, width, height);
        button.setFont(new Font("Arial", Font.PLAIN, fontSize));
        button.setBackground(Color.ORANGE);
        button.addActionListener(listener);
        return button;
    }

    /**
     * Method for creating return button in swing
     *
     * @param panelMode
     * @return returnButton
     */
    public JButton addReturnButton(PanelMode panelMode) {
        JButton returnButton = new JButton("RETURN");
        returnButton.setBounds(10, 10, 100, 50);
        returnButton.setBackground(Color.ORANGE);
        returnButton.addActionListener(e -> PanelController.start(panelMode));
        return returnButton;
    }

    /**
     * Method to simplify field creation in swing
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @return textField
     */
    public JTextField createField(int x, int y, int width, int height) {
        JTextField textField = new JTextField();
        Font font1 = new Font("font", Font.BOLD, 50);
        textField.setFont(font1);
        textField.setBounds(x, y, width, height);
        return textField;
    }

    /**
     * Method to simplify label creation in swing
     *
     * @param text
     * @param x
     * @param y
     * @param width
     * @param height
     * @return
     */
    public JLabel createLabel(String text, int x, int y, int width, int height) {
        JLabel label = new JLabel();
        label.setText(text);
        label.setBounds(x, y, width, height);
        label.setForeground(Color.WHITE);
        label.setFont(new Font("font", Font.PLAIN, 40));
        return label;
    }
}
