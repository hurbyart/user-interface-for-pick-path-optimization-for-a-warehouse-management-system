package hurbyart.simplifiedWMS.panels;

import hurbyart.simplifiedWMS.db.ConnectionDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.MapCreationMainPanel;
import hurbyart.simplifiedWMS.panels.fxDrawing.ShowMapPanel;
import hurbyart.simplifiedWMS.panels.swingDrawing.*;

import javax.swing.*;
import java.util.Objects;


public class PanelController {

    static JFrame frame;
    static MenuPanel menuPanel;

    public PanelController(String dbName) {
        frame = new JFrame("WMS with pick path optimizer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);
        frame.setVisible(true);
        menuPanel = new MenuPanel(frame);
        ConnectionDatabase.connect(dbName);
        ConnectionDatabase.createDB();

    }

    /**
     * Static method to start some panel in the program
     * @param panelMode
     */
    public static void start(PanelMode panelMode) {
        Panels panel = null;

        if (panelMode == PanelMode.MENU) {
            panel = menuPanel;
        } else if (panelMode == PanelMode.PRODUCTS_LIST) {
            panel = new ProductListPanel(frame);
        } else if (panelMode == PanelMode.MAP_CREATION) {
            panel = new MapCreationPanel(frame);
        } else if (panelMode == PanelMode.MAP_CREATION2) {
            panel = new MapCreationMainPanel(frame);
        } else if (panelMode == PanelMode.SHOW_MAP) {
            panel = new ShowMapPanel(frame);
        }
        Objects.requireNonNull(panel);
        panel.act();

    }
}
