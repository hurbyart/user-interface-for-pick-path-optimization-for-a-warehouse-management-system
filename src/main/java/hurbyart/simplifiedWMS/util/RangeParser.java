package hurbyart.simplifiedWMS.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RangeParser {

    /**
     * Static function which creates list of integers from string containing integers with , and -
     *
     * @param ranges
     * @return separatedIntegers
     */
    public static List<Integer> getDistinctNumbers(String ranges) {

        return Arrays.stream(ranges.split(","))
                .map(s -> s.replace(" ", ""))
                .map(Range::new)
                .flatMap(range -> range.render().stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    private static class Range {
        private int start;
        private int stop;

        public Range(String rangeStr) {
            String[] rangeArray = rangeStr.split("-");
            int length = rangeArray.length;

            if (length < 1 || length > 2) {
                throw new IllegalArgumentException("Wrong number of arguments in a Range: " + length);
            }

            start = Integer.parseInt(rangeArray[0]);
            stop = (length == 1) ? start : Integer.parseInt(rangeArray[1]);

            if (stop < start) {
                throw new IllegalArgumentException("Stop before start!");
            }
        }

        public List<Integer> render() {
            List<Integer> pageList = new ArrayList<>(stop - start + 1);
            for (Integer i = start; i < stop + 1; i++) {
                pageList.add(i);
            }

            return pageList;
        }
    }
}
