package hurbyart.simplifiedWMS.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class AlgorithmDurationLogger {

    public static final Logger ALGORITHM_DURATION_LOGGER = Logger.getLogger("AlgorithmDuration");

    static {
        try {
            boolean append = true;
            FileHandler fh = new FileHandler("UIPPO_AlgorithmsLogs", append);
            fh.setFormatter(new SimpleFormatter());
            ALGORITHM_DURATION_LOGGER.addHandler(fh);
            ALGORITHM_DURATION_LOGGER.info("\n\n-------------------------------------------------------\n\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

