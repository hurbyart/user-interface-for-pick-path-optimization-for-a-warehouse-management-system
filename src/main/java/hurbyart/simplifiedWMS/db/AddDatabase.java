package hurbyart.simplifiedWMS.db;

import hurbyart.simplifiedWMS.warehouseObjects.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static hurbyart.simplifiedWMS.db.ConnectionDatabase.statmt;
import static hurbyart.simplifiedWMS.db.ConnectionDatabase.conn;


public class AddDatabase {

    public static void addWarehouse(String width, String length) {
        try {
            String toInsert = "INSERT INTO 'wms' ('width','length') VALUES (" + width + "," + length + ");";
            statmt.executeUpdate(toInsert);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public static void addRack(Rack rack) {
        try {
            if (rack.getId() != 0) {
                UpdateDatabase.updateRack(rack);
                for (Slot slot : rack.getSlots()) {
                    UpdateDatabase.updateSlot(slot);
                }
                for (NodeWMS node : rack.getNodes()) {
                    UpdateDatabase.updateNode(node);
                }
            }
            String toInsert = "INSERT INTO 'rack' ('x', 'y','width','length','height','number_of_slots','two_slot_lines','horizontal','wms_id') VALUES (?,?,?,?,?,?,?,?,?);";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setDouble(1, rack.getX());
            preparedStatement.setDouble(2, rack.getY());
            preparedStatement.setDouble(3, rack.getRackSizeX());
            preparedStatement.setDouble(4, rack.getRackSizeY());
            preparedStatement.setDouble(5, rack.getRackSizeZ());
            preparedStatement.setInt(6, rack.getNumberOfSlots());
            if (rack.isTwoSlots()) preparedStatement.setString(7, "1");
            else preparedStatement.setString(7, "0");
            if (rack.isHorizontal()) preparedStatement.setString(8, "1");
            else preparedStatement.setString(8, "0");
            preparedStatement.setString(9, "1");
            preparedStatement.executeUpdate();
            String getRackId = "SELECT seq FROM sqlite_sequence WHERE name = 'rack'";
            preparedStatement = conn.prepareStatement(getRackId);
            ResultSet resSet = preparedStatement.executeQuery();
            int rackId = resSet.getInt("seq");
            rack.setId(rackId);
            for (Slot slot : rack.getSlots()) {
                addSlot(slot, rackId);
            }
            for (NodeWMS node : rack.getNodes()) {
                addNode(node, rackId);
            }
            for (Arc arc : rack.getArcs()) {
                addArc(arc, rackId);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void addNode(NodeWMS node, int rackId) {
        try {
            String toInsert = "INSERT INTO 'node' ('x','y','slot_id','rack_id') VALUES (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setDouble(1, node.getX());
            preparedStatement.setDouble(2, node.getY());
            if (rackId == 0) preparedStatement.setInt(3, 0);
            else preparedStatement.setInt(3, node.getSlot().getId());
            preparedStatement.setInt(4, rackId);
            preparedStatement.executeUpdate();
            String getNodeId = "SELECT seq FROM sqlite_sequence WHERE name = 'node'";
            preparedStatement = conn.prepareStatement(getNodeId);
            ResultSet resSet = preparedStatement.executeQuery();
            int nodeId = resSet.getInt("seq");
            node.setId(nodeId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void addArc(Arc arc, int rackId) {
        try {
            String toInsert = "INSERT INTO 'arc' ('travel_factor','head_node_id','tail_node_id','rack_id') VALUES (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setDouble(1, arc.getTravelFactor());
            preparedStatement.setInt(2, arc.getNode1().getId());
            preparedStatement.setInt(3, arc.getNode2().getId());
            preparedStatement.setInt(4, rackId);
            preparedStatement.executeUpdate();
            String getRackId = "SELECT seq FROM sqlite_sequence WHERE name = 'arc'";
            preparedStatement = conn.prepareStatement(getRackId);
            ResultSet resSet = preparedStatement.executeQuery();
            int arcId = resSet.getInt("seq");
            arc.setId(arcId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    private static void addSlot(Slot slot, int rackId) {
        try {
            String toInsert = "INSERT INTO 'slot' ('x', 'y','width','length','height','rack_id','node_id') VALUES (?,?,?,?,?,?,?);";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setDouble(1, slot.getGraphics().getRectangle().getX());
            preparedStatement.setDouble(2, slot.getGraphics().getRectangle().getY());
            preparedStatement.setDouble(3, slot.getSlotSizeX());
            preparedStatement.setDouble(4, slot.getSlotSizeY());
            preparedStatement.setDouble(5, slot.getSlotSizeZ());
            preparedStatement.setInt(6, rackId);
            preparedStatement.setInt(7, 0); // node_id, nodes are not implemented yet
            preparedStatement.executeUpdate();
            String getSlotId = "SELECT seq FROM sqlite_sequence WHERE name = 'slot'";
            preparedStatement = conn.prepareStatement(getSlotId);
            ResultSet resSet = preparedStatement.executeQuery();
            int slotId = resSet.getInt("seq");
            slot.setId(slotId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public static void addSKU(Sku sku) {
        try {
            String toInsert = "INSERT INTO 'sku' ('name','slot_id','amount_in_one_pallet','description','width','length','height') VALUES (?,?,?,?,?,?,?);";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setString(1, sku.getName());
            preparedStatement.setInt(2, sku.getSlotId());
            preparedStatement.setInt(3, sku.getAmountInOnePallet());
            preparedStatement.setString(4, sku.getDescription());
            preparedStatement.setDouble(5, sku.getWidth());
            preparedStatement.setDouble(6, sku.getLength());
            preparedStatement.setDouble(7, sku.getHeight());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
