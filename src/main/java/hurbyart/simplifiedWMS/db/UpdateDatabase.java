package hurbyart.simplifiedWMS.db;

import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Rack;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import hurbyart.simplifiedWMS.warehouseObjects.Slot;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static hurbyart.simplifiedWMS.db.ConnectionDatabase.conn;

public class UpdateDatabase {

    public static void updateWarehouseSize(String width, String length) {
        try {
            if (GetDatabase.isWarehousePlanExist() == 0) {
                AddDatabase.addWarehouse(width, length);
            }
            String sql = "UPDATE 'wms' SET width = ?, length = ?,sp_x =?, sp_y =?  WHERE wms_id = 1";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, Double.parseDouble(width));
            preparedStatement.setDouble(2, Double.parseDouble(length));
            preparedStatement.setDouble(3, 0);
            preparedStatement.setDouble(4, 0);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateSku(Sku sku) {
        try {
            String sql = "UPDATE 'sku' SET name = ?, slot_id = ?, description = ?, width = ?, length = ?, height = ?, amount_in_one_pallet = ?  WHERE sku_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, sku.getName());
            preparedStatement.setInt(2, sku.getSlotId());
            preparedStatement.setString(3, sku.getDescription());
            preparedStatement.setDouble(4, sku.getWidth());
            preparedStatement.setDouble(5, sku.getLength());
            preparedStatement.setDouble(6, sku.getHeight());
            preparedStatement.setInt(7, sku.getAmountInOnePallet());
            preparedStatement.setInt(8, sku.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateNode(NodeWMS node) {
        try {
            String sql = "UPDATE 'node' SET x = ?, y = ? WHERE node_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, node.getX());
            preparedStatement.setDouble(2, node.getY());
            preparedStatement.setInt(3, node.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateRack(Rack rack) {
        try {
            String sql = "UPDATE 'rack' SET x = ?, y = ? WHERE rack_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, rack.getX());
            preparedStatement.setDouble(2, rack.getY());
            preparedStatement.setInt(3, rack.getId());
            preparedStatement.executeUpdate();
            for (NodeWMS n : rack.getNodes()) updateNode(n);
            for (Slot s : rack.getSlots()) updateSlot(s);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateSlot(Slot slot) {

        try {
            String sql = "UPDATE 'slot' SET x = ?, y = ? WHERE slot_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, slot.getGraphics().getRectangle().getX());
            preparedStatement.setDouble(2, slot.getGraphics().getRectangle().getY());
            preparedStatement.setInt(3, slot.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public static void updateSKUSlotID(int slot_id) {
        try {
            String sql = "UPDATE 'sku' SET slot_id = ? WHERE slot_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, -1);
            preparedStatement.setInt(2, slot_id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateSpInWarehouse(double x, double y) {
        try {
            String sql = "UPDATE 'wms' SET sp_x = ?, sp_y = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, x);
            preparedStatement.setDouble(2, y);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
