package hurbyart.simplifiedWMS.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static hurbyart.simplifiedWMS.db.ConnectionDatabase.statmt;
import static hurbyart.simplifiedWMS.db.ConnectionDatabase.conn;

public class DeleteDatabase {

    public static void deleteAllData() {
        try {
            String sql = "DELETE from 'arc';" +
                    "DELETE from 'node';" +
                    "DELETE from 'rack';" +
                    "DELETE from 'sku';" +
                    "DELETE from 'slot';" +
                    "DELETE from 'sqlite_sequence';";
            statmt.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteSKU(int id) {
        try {
            String sql = "DELETE from 'sku' where sku_id=" + id;
            statmt.executeUpdate(sql);
            System.out.println("Product with id " + id + " was deleted!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteSKUbySlotId(int slotId) {
        try {
            String sql = "DELETE from 'sku' where slot_id=" + slotId;
            statmt.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteSKU(String name) {
        try {
            String sql = "DELETE from 'sku' where name=\'" + name + "\'";
            statmt.executeUpdate(sql);
            System.out.println("Product with name " + name + " was deleted!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteRackByID(int rackId) {
        try {
            String sql = "DELETE FROM rack WHERE rack_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, rackId);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteSlotByID(int slotId) {
        try {

            String sql = "DELETE FROM slot WHERE slot_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, slotId);
            UpdateDatabase.updateSKUSlotID(slotId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteSlotByRackID(int rackId) {
        try {

            String sql = "DELETE FROM slot WHERE rack_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, rackId);
            UpdateDatabase.updateSKUSlotID(rackId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteArcByRackID(int rackId) {
        try {

            String sql = "DELETE FROM arc WHERE rack_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, rackId);
            UpdateDatabase.updateSKUSlotID(rackId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }




    public static void deleteNodeByRackID(int rackId) {
        try {

            String sql = "DELETE FROM node WHERE rack_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, rackId);
            UpdateDatabase.updateSKUSlotID(rackId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteNodeByID(int nodeId) {

        try {
            String sql1 = "DELETE FROM node WHERE node_id = ?;" ;
            String sql2 = "DELETE FROM arc WHERE head_node_id = ? OR tail_node_id = ?;";
            PreparedStatement preparedStatement1 = conn.prepareStatement(sql1);
            PreparedStatement preparedStatement2 = conn.prepareStatement(sql2);
            preparedStatement1.setInt(1, nodeId);
            preparedStatement2.setInt(1, nodeId);
            preparedStatement2.setInt(2, nodeId);
            preparedStatement1.executeUpdate();
            preparedStatement2.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteArcByID(int arcId) {
        try {
            String sql = "DELETE FROM arc WHERE arc_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, arcId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
