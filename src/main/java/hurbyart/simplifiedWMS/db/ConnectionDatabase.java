package hurbyart.simplifiedWMS.db;

import java.sql.*;

public class ConnectionDatabase {
    static Connection conn;
    static Statement statmt;
    static ResultSet resSet;

    public static final String DATABASE_DEFAULT_NAME = "UIPPO_DB";

    public static void connect(String dbName) {
        try {
            conn = null;
            Class.forName("org.sqlite.JDBC");
            if(dbName != null){
                if(dbName.equals("firstPlan") || dbName.equals("secondPlan") || dbName.equals("thirdPlan")) {
                    conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
                } else {
                    System.out.println("Wrong loading database name!\nLoading from default.");
                    conn = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_DEFAULT_NAME);
                }
            } else {
                conn = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_DEFAULT_NAME);
            }

            System.out.println("Database connected!");
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }


    public static void createDB() {
        try {
            statmt = conn.createStatement();
            statmt.execute("CREATE TABLE if not exists 'wms' ('wms_id' INTEGER PRIMARY KEY AUTOINCREMENT,'width' decimal(10,2) NOT NULL DEFAULT '0.00','length' decimal(10,2) NOT NULL DEFAULT '0.00','sp_x' decimal(10,2)  ,'sp_y' decimal(10,2) );");
            statmt.execute("CREATE TABLE if not exists 'node' ('node_id' INTEGER PRIMARY KEY AUTOINCREMENT,'x' decimal(10,2) NOT NULL DEFAULT '0.00','y' decimal(10,2) NOT NULL DEFAULT '0.00', 'slot_id' INTEGER , 'rack_id' INTEGER , FOREIGN KEY (slot_id) REFERENCES slot(slot_id), FOREIGN KEY (rack_id) REFERENCES rack(rack_id));");
            statmt.execute("CREATE TABLE if not exists 'rack' ('rack_id' INTEGER PRIMARY KEY AUTOINCREMENT,'x' decimal(10,2) NOT NULL ,'y' decimal(10,2) NOT NULL , 'width' decimal(10,2) NOT NULL ,'length' decimal(10,2) NOT NULL,'height' decimal(10,2) NOT NULL, 'number_of_slots' INTEGER NOT NULL, 'two_slot_lines' INTEGER NOT NULL,'horizontal' INTEGER NOT NULL, 'wms_id' INTEGER NOT NULL, FOREIGN KEY (wms_id) REFERENCES wms(wms_id));");
            statmt.execute("CREATE TABLE if not exists 'slot' ('slot_id' INTEGER PRIMARY KEY AUTOINCREMENT,'x' decimal(10,2) NOT NULL ,'y' decimal(10,2) NOT NULL ,'width' decimal(10,2) NOT NULL ,'length' decimal(10,2) NOT NULL,'height' decimal(10,2) NOT NULL,'rack_id' INTEGER NOT NULL,'node_id' INTEGER NOT NULL, FOREIGN KEY (node_id) REFERENCES node(node_id),FOREIGN KEY (rack_id) REFERENCES rack(rack_id));");
            statmt.execute("CREATE TABLE if not exists 'sku' ('sku_id' INTEGER PRIMARY KEY AUTOINCREMENT,  'name' text NOT NULL, 'slot_id' INTEGER NOT NULL, 'description' text DEFAULT NULL,'width' decimal(10,2)  ,'length' decimal(10,2),'height' decimal(10,2) ,'amount_in_one_pallet' INTEGER ,FOREIGN KEY (slot_id) REFERENCES slot(slot_id));");
            statmt.execute("CREATE TABLE if not exists 'arc' ('arc_id' INTEGER PRIMARY KEY AUTOINCREMENT ,'travel_factor' decimal(5,2) NOT NULL,'head_node_id' INTEGER NOT NULL,'tail_node_id' INTEGER NOT NULL, 'rack_id' INTEGER ,FOREIGN KEY (head_node_id) REFERENCES node(node_id), FOREIGN KEY (tail_node_id) REFERENCES node(node_id), FOREIGN KEY (rack_id) REFERENCES rack(rack_id));");

            System.out.println("Database is created or exist!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void closeDB() {
        try {
            conn.close();
            statmt.close();
            resSet.close();

            System.out.println("Database is closed!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
