package hurbyart.simplifiedWMS.db;

import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.StartingPointGraphics;
import hurbyart.simplifiedWMS.warehouseObjects.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static hurbyart.simplifiedWMS.db.ConnectionDatabase.statmt;
import static hurbyart.simplifiedWMS.db.ConnectionDatabase.conn;

public class GetDatabase {

    public static List<Rack> getAllRacks(int wms_id) {
        try {
            String toInsert = "SELECT * FROM rack WHERE wms_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, wms_id);
            ResultSet resSet = preparedStatement.executeQuery();
            List<Rack> racks = new ArrayList<>();
            while (resSet.next()) {
                int id = resSet.getInt("rack_id");
                double x = resSet.getDouble("x");
                double y = resSet.getDouble("y");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int numberOfSlots = resSet.getInt("number_of_slots");
                int twoSlotLines = resSet.getInt("two_slot_lines");
                int horizontal = resSet.getInt("horizontal");
                racks.add(new Rack(id, width, length, height, numberOfSlots, twoSlotLines == 1, horizontal == 1, x, y));
            }
            return racks;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Rack getRackByRackId(int rackId) {
        try {

            String toInsert = "SELECT * FROM rack WHERE rack_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, rackId);
            ResultSet resSet = preparedStatement.executeQuery();
            List<Rack> racks = new ArrayList<>();
            while (resSet.next()) {
                int id = resSet.getInt("rack_id");
                double x = resSet.getDouble("x");
                double y = resSet.getDouble("y");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int numberOfSlots = resSet.getInt("number_of_slots");
                int twoSlotLines = resSet.getInt("two_slot_lines");
                int horizontal = resSet.getInt("horizontal");
                racks.add(new Rack(id, width, length, height, numberOfSlots, twoSlotLines == 1, horizontal == 1, x, y));
            }
            return racks.get(0);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Slot getSlotBySlotId(int slotId) {
        try {
            String toInsert = "SELECT * FROM slot WHERE slot_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, slotId);
            ResultSet resSet = preparedStatement.executeQuery();
            Slot slot = null;
            while (resSet.next()) {
                int id = resSet.getInt("slot_id");
                double x = resSet.getDouble("x");
                double y = resSet.getDouble("y");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int rackId = resSet.getInt("rack_id");
                slot = new Slot(id, x, y, width, length, height, rackId);
            }
            return slot;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<Slot> getAllSlots() {
        try {
            String toInsert = "SELECT * FROM slot";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            ResultSet resSet = preparedStatement.executeQuery();
            List<Slot> slots = new ArrayList<>();
            while (resSet.next()) {
                int id = resSet.getInt("slot_id");
                double x = resSet.getDouble("x");
                double y = resSet.getDouble("y");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int rackId = resSet.getInt("rack_id");
                Slot slot = new Slot(id, x, y, width, length, height, rackId);
                slots.add(slot);
            }
            return slots;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<Slot> getAllSlotsByRackId(int rackId) {
        try {
            String toInsert = "SELECT * FROM slot WHERE rack_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, rackId);
            ResultSet resSet2 = preparedStatement.executeQuery();
            List<Slot> slots = new ArrayList<>();
            while (resSet2.next()) {
                int id = resSet2.getInt("slot_id");
                double x = resSet2.getDouble("x");
                double y = resSet2.getDouble("y");
                double width = resSet2.getDouble("width");
                double length = resSet2.getDouble("length");
                double height = resSet2.getDouble("height");

                Slot slot = new Slot(id, x, y, width, length, height, rackId);
                slots.add(slot);
            }
            return slots;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<NodeWMS> getAllNodes() {
        try {
            String toInsert = "SELECT * FROM node";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            ResultSet resSet2 = preparedStatement.executeQuery();
            List<NodeWMS> nodes = new ArrayList<>();
            while (resSet2.next()) {
                int id = resSet2.getInt("node_id");
                double x = resSet2.getDouble("x");
                double y = resSet2.getDouble("y");
                int slotId = resSet2.getInt("slot_id");
                int rackId = resSet2.getInt("rack_id");
                Slot slot = getSlotBySlotId(slotId);
                NodeWMS node = new NodeWMS(id, x, y, slot, rackId);
                nodes.add(node);
            }
            return nodes;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<NodeWMS> getAllNodesByRackId(int rackId) {
        try {
            String toInsert = "SELECT * FROM node WHERE rack_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, rackId);
            ResultSet resSet2 = preparedStatement.executeQuery();
            List<NodeWMS> nodes = new ArrayList<>();
            while (resSet2.next()) {
                int id = resSet2.getInt("node_id");
                double x = resSet2.getDouble("x");
                double y = resSet2.getDouble("y");
                int slotId = resSet2.getInt("slot_id");
                Slot slot = getSlotBySlotId(slotId);
                NodeWMS node = new NodeWMS(id, x, y, slot, rackId);
                nodes.add(node);
            }
            return nodes;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static NodeWMS getNodeById(int nodeId) {
        if (nodeId == -1) {
            StartingPointGraphics spg = new StartingPointGraphics();
            return new NodeWMS(spg);
        }
        try {
            String toInsert = "SELECT * FROM node WHERE node_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, nodeId);
            ResultSet resSet2 = preparedStatement.executeQuery();
            NodeWMS node = null;
            while (resSet2.next()) {
                int id = resSet2.getInt("node_id");
                double x = resSet2.getDouble("x");
                double y = resSet2.getDouble("y");
                int slotId = resSet2.getInt("slot_id");
                int rackId = resSet2.getInt("rack_id");
                Slot slot = getSlotBySlotId(slotId);
                node = new NodeWMS(id, x, y, slot, rackId);
            }
            return node;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static List<Arc> getAllArcsByRackId(int rackId) {

        try {
            String toInsert = "SELECT * FROM arc WHERE rack_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, rackId);
            ResultSet resSet2 = preparedStatement.executeQuery();
            List<Arc> arcs = new ArrayList<>();
            while (resSet2.next()) {
                int id = resSet2.getInt("arc_id");
                int nodeId1 = resSet2.getInt("head_node_id");
                int nodeId2 = resSet2.getInt("tail_node_id");
                double travelFactor = resSet2.getDouble("travel_factor");
                NodeWMS node1 = getNodeById(nodeId1);
                NodeWMS node2 = getNodeById(nodeId2);
                Arc arc = new Arc(id, node1, node2, travelFactor, rackId);
                arcs.add(arc);
            }
            return arcs;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<Arc> getAllArcs() {
        try {
            String toInsert = "SELECT * FROM arc";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            ResultSet resSet2 = preparedStatement.executeQuery();
            List<Arc> arcs = new ArrayList<>();
            while (resSet2.next()) {
                int id = resSet2.getInt("arc_id");
                int nodeId1 = resSet2.getInt("head_node_id");
                int nodeId2 = resSet2.getInt("tail_node_id");
                int rackId = resSet2.getInt("rack_id");
                double travelFactor = resSet2.getDouble("travel_factor");
                NodeWMS node1 = getNodeById(nodeId1);
                NodeWMS node2 = getNodeById(nodeId2);
                Arc arc = new Arc(id, node1, node2, travelFactor, rackId);
                arcs.add(arc);
            }
            return arcs;
        } catch (
                SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Arc getArcById(int arcId) {
        try {
            String toInsert = "SELECT * FROM arc WHERE arc_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, arcId);
            ResultSet resSet2 = preparedStatement.executeQuery();

            while (resSet2.next()) {
                int id = resSet2.getInt("arc_id");
                int nodeId1 = resSet2.getInt("head_node_id");
                int nodeId2 = resSet2.getInt("tail_node_id");
                int rackId = resSet2.getInt("rack_id");
                double travelFactor = resSet2.getDouble("travel_factor");
                NodeWMS node1 = getNodeById(nodeId1);
                NodeWMS node2 = getNodeById(nodeId2);
                Arc arc = new Arc(id, node1, node2, travelFactor, rackId);
                return arc;
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static int isWarehousePlanExist() {
        try {
            ResultSet resSet = statmt.executeQuery("SELECT COUNT( *)AS rowcount FROM wms");
            resSet.next();
            return resSet.getInt("rowcount");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static double[] getWarehouseSize() {
        if (isWarehousePlanExist() == 0) {
            System.out.println("WMS size wasn't added");
            return null;
        }
        try {
            ResultSet resSet = statmt.executeQuery("SELECT * FROM wms");
            double[] data = new double[2];
            resSet.next();
            data[0] = resSet.getDouble("width");
            data[1] = resSet.getDouble("length");
            return data;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static List<Sku> getAllSKUInSlot(int slotId) {
        try {
            String toInsert = "SELECT * FROM sku WHERE slot_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, slotId);
            ResultSet resSet = preparedStatement.executeQuery();
            List<Sku> skus = new ArrayList<>();
            while (resSet.next()) {
                int id = resSet.getInt("sku_id");
                String name = resSet.getString("name");
                String description = resSet.getString("description");
                int amountInOnePallet = resSet.getInt("amount_in_one_pallet");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                Sku sku = new Sku(id, name, slotId, amountInOnePallet, description);
                sku.setWidth(width);
                sku.setLength(length);
                sku.setHeight(height);
                skus.add(sku);
            }
            return skus;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Sku getOneSKUInSlot(int slotId) {
        try {
            String toInsert = "SELECT * FROM sku WHERE slot_id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, slotId);
            ResultSet resSet = preparedStatement.executeQuery();
            Sku sku = null;
            while (resSet.next()) {
                int id = resSet.getInt("sku_id");
                String name = resSet.getString("name");
                String description = resSet.getString("description");
                int amountInOnePallet = resSet.getInt("amount_in_one_pallet");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                sku = new Sku(id, name, slotId, amountInOnePallet, description);
                sku.setWidth(width);
                sku.setLength(length);
                sku.setHeight(height);
            }
            return sku;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    private static int getItemsAmount() {
        try {
            ResultSet resSet2 = statmt.executeQuery("SELECT COUNT( *)AS rowcount FROM sku");
            resSet2.next();
            return resSet2.getInt("rowcount");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }


    public static Object[][] getAllProducts() {
        try {
            int rowsNumber = getItemsAmount();
            ResultSet resSet = statmt.executeQuery("SELECT * FROM sku");

            Object[][] data = new Object[rowsNumber][8];
            int i = 0;
            while (resSet.next()) {
                int id = resSet.getInt("sku_id");
                String name = resSet.getString("name");
                int slot_id = resSet.getInt("slot_id");
                String description = resSet.getString("description");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int amountInOnePallet = resSet.getInt("amount_in_one_pallet");
                Object[] product = new Object[]{id, name, slot_id, description, width, length, height, amountInOnePallet};
                data[i] = product;
                i++;
            }
            return data;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Sku getSkuById(int id) {
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM sku WHERE sku_id=?");
            ps.setInt(1, id);
            ResultSet resSet = ps.executeQuery();

            while (resSet.next()) {
                String name = resSet.getString("name");
                int slot_id = resSet.getInt("slot_id");
                String description = resSet.getString("description");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int amountInOnePallet = resSet.getInt("amount_in_one_pallet");
                Sku sku = new Sku(id, name, slot_id, amountInOnePallet, description);
                sku.setWidth(width);
                sku.setLength(length);
                sku.setHeight(height);
                return sku;
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Sku getSkuByName(String name) {
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM sku WHERE name=?");
            ps.setString(1, name);
            ResultSet resSet = ps.executeQuery();

            while (resSet.next()) {
                int id = resSet.getInt("sku_id");
                int slot_id = resSet.getInt("slot_id");
                String description = resSet.getString("description");
                double width = resSet.getDouble("width");
                double length = resSet.getDouble("length");
                double height = resSet.getDouble("height");
                int amountInOnePallet = resSet.getInt("amount_in_one_pallet");
                Sku sku = new Sku(id, name, slot_id, amountInOnePallet, description);
                sku.setWidth(width);
                sku.setLength(length);
                sku.setHeight(height);
                return sku;
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Double[] getStartingPointCoordinates() {
        try {
            Double[] coordinates = new Double[2];

            String query = "SELECT * FROM wms";
            ResultSet rs = statmt.executeQuery(query);
            while (rs.next()) {
                coordinates[0] = rs.getDouble("sp_x");
                coordinates[1] = rs.getDouble("sp_y");
            }
            return coordinates;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean isSpConnected() {
        try {
            String query = "SELECT * FROM arc WHERE head_node_id = -1 OR tail_node_id = -1";
            ResultSet rs = statmt.executeQuery(query);
            return rs.next();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static List<NodeWMS> getNodesBySlotId(int slotId) {
        try {
            String toInsert = "SELECT * FROM node WHERE slot_id=?";
            List<NodeWMS> toRet = new ArrayList<>();
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, slotId);
            ResultSet resSet2 = preparedStatement.executeQuery();
            while (resSet2.next()) {
                int id = resSet2.getInt("node_id");
                double x = resSet2.getDouble("x");
                double y = resSet2.getDouble("y");
                int rackId = resSet2.getInt("rack_id");
                Slot slot = getSlotBySlotId(slotId);
                NodeWMS node = new NodeWMS(id, x, y, slot, rackId);
                toRet.add(node);
            }
            return toRet;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * @param nodeId1
     * @param nodeId2
     * @return -1 if not exist, otherwise arcId
     * @throws SQLException
     */
    public static Arc ifExistArcBetweenTwoNodes(int nodeId1, int nodeId2) {
        try {
            String toInsert = "SELECT * FROM arc WHERE (head_node_id=? AND tail_node_id=?) OR (head_node_id=? AND tail_node_id=?)";
            PreparedStatement preparedStatement = conn.prepareStatement(toInsert);
            preparedStatement.setInt(1, nodeId1);
            preparedStatement.setInt(2, nodeId2);
            preparedStatement.setInt(3, nodeId2);
            preparedStatement.setInt(4, nodeId1);
            ResultSet resSet2 = preparedStatement.executeQuery();
            while (resSet2.next()) {
                int arcId = resSet2.getInt("arc_id");
                Arc arc = getArcById(arcId);
                return arc;
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
