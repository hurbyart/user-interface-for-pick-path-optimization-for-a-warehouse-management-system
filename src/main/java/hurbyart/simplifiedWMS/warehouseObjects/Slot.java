package hurbyart.simplifiedWMS.warehouseObjects;

import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.SlotGraphics;
import javafx.scene.shape.Rectangle;


public class Slot {

    private int id;
    private double slotSizeX;
    private double slotSizeY;
    private double slotSizeZ;
    private int rackId;
    private SlotGraphics graphics;
    private int palletsAmountInSlot;


    public final static double PALLET_WIDTH = 0.8;
    public final static double PALLET_LENGTH = 0.12;
    public final static double PALLET_HEIGHT = 2;


    public Slot(int id, double x, double y, double slotSizeX, double slotSizeY, double slotSizeZ, int rackId) {
        this.id = id;
        this.slotSizeX = slotSizeX;
        this.slotSizeY = slotSizeY;
        this.slotSizeZ = slotSizeZ;
        this.graphics = new SlotGraphics(this, new Rectangle(x, y, slotSizeX, slotSizeY));
        this.rackId = rackId;
        int palletsX = (int) (slotSizeX / PALLET_WIDTH);
        int palletsY = (int) (slotSizeY / PALLET_LENGTH);
        int palletsZ = (int) (slotSizeZ / PALLET_HEIGHT);
        this.palletsAmountInSlot = palletsX * palletsY * palletsZ;
        ;
    }


    public int getId() {
        return id;
    }


    public double getSlotSizeX() {
        return slotSizeX;
    }


    public double getSlotSizeY() {
        return slotSizeY;
    }


    public double getSlotSizeZ() {
        return slotSizeZ;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setSlotX(double x) {
        graphics.getRectangle().setX(x);
    }

    public void setSlotY(double y) {
        graphics.getRectangle().setY(y);
    }

    public double getSlotX() {
        return graphics.getRectangle().getX();
    }

    public double getSlotY() {
        return graphics.getRectangle().getY();
    }

    public int getRackId() {
        return rackId;
    }

    public int getPalletsAmountInSlot() {
        return palletsAmountInSlot;
    }

    public SlotGraphics getGraphics() {
        return graphics;
    }
}
