package hurbyart.simplifiedWMS.warehouseObjects;

import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.NodeGraphics;
import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.StartingPointGraphics;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.HashMap;

import static hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.NodeGraphics.NODE_RADIUS;

public class NodeWMS {

    private int id;
    private Slot slot;
    private int rackId;
    private NodeGraphics graphics;
    private HashMap<NodeWMS, Double> neighbours = new HashMap<>();

    public NodeWMS(int id, double x, double y, Slot slot, int rackId) {
        this.id = id;

        this.slot = slot;
        this.rackId = rackId;
        this.graphics = new NodeGraphics(this, new Circle(x, y, NODE_RADIUS, Color.DARKBLUE));
    }


    public NodeWMS(StartingPointGraphics spg) {
        this.id = -1;
        this.slot = null;
        this.rackId = 0;
        this.graphics = spg;
    }

    public HashMap<NodeWMS, Double> getNeighbours() {
        return neighbours;
    }

    public int getId() {
        return id;
    }

    public double getX() {
        return graphics.getCircle().getCenterX();
    }

    public double getY() {
        return graphics.getCircle().getCenterY();
    }

    public Slot getSlot() {
        return slot;
    }

    public int getRackId() {
        return rackId;
    }

    public NodeGraphics getGraphics() {
        return graphics;
    }

    public void setX(double x) {
        graphics.getCircle().setCenterX(x);
    }

    public void setY(double y) {
        graphics.getCircle().setCenterY(y);
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof NodeWMS)) return false;
        NodeWMS anotherNode = (NodeWMS) object;
        if (id != anotherNode.id) return false;
        if (rackId != anotherNode.rackId) return false;
        if (graphics.getCircle().getCenterX() != anotherNode.graphics.getCircle().getCenterX()) return false;
        if (graphics.getCircle().getCenterY() != anotherNode.graphics.getCircle().getCenterY()) return false;
        return true;
    }
}
