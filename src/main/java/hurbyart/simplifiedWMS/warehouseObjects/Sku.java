package hurbyart.simplifiedWMS.warehouseObjects;

public class Sku {

    private int id;
    private String name;
    private int amountInOnePallet;
    private int slotId;
    private String description;
    private double width;
    private double length;
    private double height;


    public Sku(int id, String name, int slotId, int amountInOnePallet, String description) {
        this.id = id;
        this.name = name;
        this.slotId = slotId;
        this.amountInOnePallet = amountInOnePallet;
        this.description = description;
        this.width = 0;
        this.length = 0;
        this.height = 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmountInOnePallet() {
        return amountInOnePallet;
    }

    public int getSlotId() {
        return slotId;
    }

    public String getDescription() {
        return description;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmountInOnePallet(int amountInOnePallet) {
        this.amountInOnePallet = amountInOnePallet;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
