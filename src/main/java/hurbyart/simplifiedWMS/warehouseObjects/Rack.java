package hurbyart.simplifiedWMS.warehouseObjects;

import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.RackGraphics;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.RackGraphics.RACK_STROKE_SIZE;

public class Rack {


    public final static double DISTANCE_BETWEEN_NODE_AND_SLOT = 5;

    private int id;
    private double rackSizeX;
    private double rackSizeY;
    private double rackSizeZ;
    private int numberOfSlots;
    private boolean twoSlots;
    private double slotSize;
    private boolean horizontal;
    private RackGraphics graphics;

    List<Slot> slots;
    List<NodeWMS> nodes;
    List<Arc> arcs;


    public Rack(int id, double rackSizeX, double rackSizeY, double rackSizeZ, int numberOfSlots, boolean twoSlots, boolean horizontal, double x, double y) {
        this.id = id;
        this.rackSizeX = rackSizeX;
        this.rackSizeY = rackSizeY;
        this.rackSizeZ = rackSizeZ;
        this.numberOfSlots = numberOfSlots;
        this.twoSlots = twoSlots;
        this.horizontal = horizontal;
        this.slotSize = horizontal ? rackSizeX / numberOfSlots : rackSizeY / numberOfSlots;
        if (id == 0) {
            addSlotsNodesAndArcs(x, y);
        } else {
            this.slots = GetDatabase.getAllSlotsByRackId(id);
            this.nodes = GetDatabase.getAllNodesByRackId(id);
            this.arcs = GetDatabase.getAllArcsByRackId(id);
        }
        this.graphics = new RackGraphics(this, new Rectangle(x, y, rackSizeX - RACK_STROKE_SIZE, rackSizeY - RACK_STROKE_SIZE));
    }


    private void addSlotsNodesAndArcs(double x, double y) {
        this.slots = new ArrayList<>();
        this.nodes = new ArrayList<>();
        this.arcs = new ArrayList<>();
        NodeWMS tmpNode1 = null;
        NodeWMS tmpNode2 = null;
        double[] warehouseSize = GetDatabase.getWarehouseSize();
        Objects.requireNonNull(warehouseSize);
        for (int i = 0; i < numberOfSlots; i++) {
            NodeWMS node1 = null;
            NodeWMS node2 = null;
            if (horizontal) {

                if (twoSlots) {
                    Slot slot = new Slot(0, x + i * slotSize, y, slotSize, rackSizeY / 2, rackSizeZ, id);
                    slots.add(slot);
                    if (y - DISTANCE_BETWEEN_NODE_AND_SLOT >= 0)
                        node1 = addNode(x + i * slotSize + slotSize / 2, y - DISTANCE_BETWEEN_NODE_AND_SLOT, slot);
                    slot = new Slot(0, x + i * slotSize, y + rackSizeY / 2, slotSize, rackSizeY / 2, rackSizeZ, id);
                    slots.add(slot);
                    if (y + rackSizeY + DISTANCE_BETWEEN_NODE_AND_SLOT <= warehouseSize[1])
                        node2 = addNode(x + i * slotSize + slotSize / 2, y + rackSizeY + DISTANCE_BETWEEN_NODE_AND_SLOT, slot);
                } else {
                    Slot slot = new Slot(0, x + i * slotSize, y, slotSize, rackSizeY, rackSizeZ, id);
                    slots.add(slot);
                    if (y - DISTANCE_BETWEEN_NODE_AND_SLOT >= 0)
                        node1 = addNode(x + i * slotSize + slotSize / 2, y - DISTANCE_BETWEEN_NODE_AND_SLOT, slot);
                    if (y + rackSizeY + DISTANCE_BETWEEN_NODE_AND_SLOT <= warehouseSize[1])
                        node2 = addNode(x + i * slotSize + slotSize / 2, y + rackSizeY + DISTANCE_BETWEEN_NODE_AND_SLOT, slot);
                }

            } else {

                if (twoSlots) {
                    Slot slot = new Slot(0, x, y + i * slotSize, rackSizeX / 2, slotSize, rackSizeZ, id);
                    slots.add(slot);
                    if (x - DISTANCE_BETWEEN_NODE_AND_SLOT >= 0)
                        node1 = addNode(x - DISTANCE_BETWEEN_NODE_AND_SLOT, y + i * slotSize + slotSize / 2, slot);
                    slot = new Slot(0, x + rackSizeX / 2, y + i * slotSize, rackSizeX / 2, slotSize, rackSizeZ, id);
                    slots.add(slot);
                    if (x + rackSizeX + DISTANCE_BETWEEN_NODE_AND_SLOT <= warehouseSize[0])
                        node2 = addNode(x + rackSizeX + DISTANCE_BETWEEN_NODE_AND_SLOT, y + i * slotSize + slotSize / 2, slot);
                } else {
                    Slot slot = new Slot(0, x, y + i * slotSize, rackSizeX, slotSize, rackSizeZ, id);
                    slots.add(slot);
                    if (x - DISTANCE_BETWEEN_NODE_AND_SLOT >= 0)
                        node1 = addNode(x - DISTANCE_BETWEEN_NODE_AND_SLOT, y + i * slotSize + slotSize / 2, slot);
                    if (x + rackSizeX + DISTANCE_BETWEEN_NODE_AND_SLOT <= warehouseSize[0])
                        node2 = addNode(x + rackSizeX + DISTANCE_BETWEEN_NODE_AND_SLOT, y + i * slotSize + slotSize / 2, slot);
                }
            }
            if (i != 0) {
                if (node1 != null) {
                    addArc(tmpNode1, node1);
                }
                if (node2 != null) {
                    addArc(tmpNode2, node2);
                }
            }
            tmpNode1 = node1;
            tmpNode2 = node2;
        }

    }


    private NodeWMS addNode(double x, double y, Slot slot) {
        NodeWMS node = new NodeWMS(0, x, y, slot, id);
        nodes.add(node);
        return node;
    }

    private void addArc(NodeWMS node1, NodeWMS node2) {
        Arc arc = new Arc(0, node1, node2, id);
        arcs.add(arc);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public List<NodeWMS> getNodes() {
        return nodes;
    }

    public List<Arc> getArcs() {
        return arcs;
    }

    public double getRackSizeX() {
        return rackSizeX;
    }

    public double getRackSizeY() {
        return rackSizeY;
    }

    public double getRackSizeZ() {
        return rackSizeZ;
    }

    public int getNumberOfSlots() {
        return numberOfSlots;
    }

    public boolean isTwoSlots() {
        return twoSlots;
    }


    public boolean isHorizontal() {
        return horizontal;
    }

    public double getSlotSize() {
        return slotSize;
    }

    public RackGraphics getGraphics() {
        return graphics;
    }

    public void moveXandY(double x, double y) {
        double changeX = graphics.moveX(x);
        double changeY = graphics.moveY(y);
        for (Slot slot : slots) {
            slot.setSlotX(slot.getSlotX() - changeX);
            slot.setSlotY(slot.getSlotY() - changeY);
        }
        for (NodeWMS node : nodes) {
            node.setX(node.getX() - changeX);
            node.setY(node.getY() - changeY);
        }
        for (Arc arc : arcs) {
            arc.update();
        }
    }

    public void moveX(double x) {

        double change = graphics.moveX(x);
        for (Slot slot : slots) {
            slot.setSlotX(slot.getSlotX() - change);
        }
        for (NodeWMS node : nodes) {
            node.setX(node.getX() - change);
        }
        for (Arc arc : arcs) {
            arc.update();
        }
    }

    public void moveY(double y) {
        double change = graphics.moveY(y);
        for (Slot slot : slots) {
            slot.setSlotY(slot.getSlotY() - change);
        }
        for (NodeWMS node : nodes) {
            node.setY(node.getY() - change);
        }
        for (Arc arc : arcs) {
            arc.update();
        }
    }

    public double getX() {
        return graphics.getRectangle().getX();
    }

    public double getY() {
        return graphics.getRectangle().getY();
    }

}
