package hurbyart.simplifiedWMS.warehouseObjects;

import hurbyart.simplifiedWMS.db.GetDatabase;
import hurbyart.simplifiedWMS.panels.fxDrawing.warehouseObjectsGraphics.ArcGraphics;
import javafx.scene.shape.Line;

public class Arc {

    private int id;
    private NodeWMS node1;
    private NodeWMS node2;
    private double travelFactor;
    private int rackId;
    private ArcGraphics graphics;

    public Arc(int id, NodeWMS node1, NodeWMS node2, int rackId) {
        this.id = id;
        this.node1 = node1;
        this.node2 = node2;
        this.travelFactor = calculateTravelFactor();
        this.rackId = rackId;
        this.graphics = new ArcGraphics(this, new Line(node1.getX(), node1.getY(), node2.getX(), node2.getY()));
    }

    public Arc(int id, NodeWMS node1, NodeWMS node2, double travelFactor, int rackId) {
        this.id = id;
        this.node1 = node1;
        this.node2 = node2;
        this.travelFactor = travelFactor;
        this.rackId = rackId;
        this.graphics = new ArcGraphics(this, new Line(node1.getX(), node1.getY(), node2.getX(), node2.getY()));
    }

    private double calculateTravelFactor() {
        double x = Math.pow((node2.getX() - node1.getX()), 2);
        double y = Math.pow((node2.getY() - node1.getY()), 2);
        return Math.sqrt(x + y);
    }

    public void update() {
        if (node1.getId() == -1 || node2.getId() == -1) {
            Double[] coordinates = GetDatabase.getStartingPointCoordinates();
            if (node1.getId() == -1) {
                node1.setX(coordinates[0]);
                node1.setY(coordinates[1]);
            } else {
                node2.setX(coordinates[0]);
                node2.setY(coordinates[1]);
            }
        }
        graphics.getLine().setStartX(node1.getX());
        graphics.getLine().setEndX(node2.getX());
        graphics.getLine().setStartY(node1.getY());
        graphics.getLine().setEndY(node2.getY());
        this.travelFactor = calculateTravelFactor();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NodeWMS getNode1() {
        return node1;
    }

    public NodeWMS getNode2() {
        return node2;
    }

    public int getRackId() {
        return rackId;
    }

    public double getTravelFactor() {
        return travelFactor;
    }

    public ArcGraphics getGraphics() {
        return graphics;
    }
}
