package hurbyart.simplifiedWMS.algorithms.psjava;


import hurbyart.simplifiedWMS.algorithms.Algorithm;
import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import javafx.util.Pair;
import org.psjava.algo.graph.shortestpath.AllPairShortestPathResult;
import org.psjava.algo.graph.shortestpath.FloydWarshallAlgorithm;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodJohnsonAlgorithm;

import java.util.*;

public class PSJavaAlgorithm extends Algorithm {

    MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();
    AllPairShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> res;


    public PSJavaAlgorithm(NodeWMS spNode, List<NodeWMS> nodes, List<Arc> arcs, boolean johnsonAlgorithm) {
        super(spNode, nodes, arcs);
        for (Arc arc : arcs) {
            graph.insertVertex(Integer.toString(arc.getNode1().getId()));
            graph.insertVertex(Integer.toString(arc.getNode2().getId()));
            int weight = (int) (arc.getTravelFactor() * 100000);
            graph.addEdge(Integer.toString(arc.getNode1().getId()), Integer.toString(arc.getNode2().getId()), weight);
            graph.addEdge(Integer.toString(arc.getNode2().getId()), Integer.toString(arc.getNode1().getId()), weight);
        }


        if (johnsonAlgorithm) res = GoodJohnsonAlgorithm.getInstance().calc(graph, IntegerNumberSystem.getInstance());
        else res = FloydWarshallAlgorithm.getInstance().calc(graph, IntegerNumberSystem.getInstance());
    }

    /**
     * Method to create path using Floyd Warshall/Johnson algorithm
     *
     * @param skus
     * @param useSpAsStart
     * @param useSpAsEnd
     * @return pair which contains arcs to create path and path length
     */
    public Pair<List<List<Arc>>, Double> createPath(List<Sku> skus, boolean useSpAsStart, boolean useSpAsEnd) {
        List<List<Arc>> arcsToDraw = new ArrayList<>();


        List<String> nodesInOrder = new ArrayList<>();
        for (Sku sku : skus) {
            nodesInOrder.add(Integer.toString(findNodesWithSlotId(sku.getSlotId()).get(0).getId()));
        }
        String startNode = useSpAsStart ? Integer.toString(spNode.getId()) : null;
        String endNode = useSpAsEnd ? Integer.toString(spNode.getId()) : null;
        Pair<List<List>, Double> res = execute(startNode, endNode, nodesInOrder);
        if (res == null) return null;
        for (List<String> l : res.getKey()) {
            List<Arc> arcs = new ArrayList<>();
            for (int i = 0; i < l.size() - 1; i++) {
                Arc arc = findArcByNodeIds(Integer.parseInt(l.get(i)), Integer.parseInt(l.get(i + 1)));
                if (arc != null) arcs.add(arc);
            }
            arcsToDraw.add(arcs);
        }

        return new Pair<>(arcsToDraw, res.getValue());
    }

    private Pair<List<List>, Double> execute(String source, String goal, List<String> mustVisitNodes) {


        double answer = Double.MAX_VALUE;
        List<List> answerPath = null;
        List<List<String>> permutationOfMustVisitNodes = generatePerm(mustVisitNodes);
        for (List<String> l : permutationOfMustVisitNodes) {
            List<List> path = new ArrayList<>();
            double currentAnswer = 0.0;
            Pair<List<String>, Double> res;
            List<String> toAdd;
            if (source != null) {
                res = findDistanceBetween(source, l.get(0));
                if (res == null) return null;
                currentAnswer += res.getValue();
                toAdd = new ArrayList<>(res.getKey());
                toAdd.add(l.get(0));
                path.add(toAdd);
            }
            for (int i = 0; i < l.size() - 1; i++) {
                res = findDistanceBetween(l.get(i), l.get(i + 1));
                if (res == null) return null;
                currentAnswer += res.getValue();
                path.add(res.getKey());
            }
            if (goal != null) {
                res = findDistanceBetween(l.get(l.size() - 1), goal);
                if (res == null) return null;
                currentAnswer += res.getValue();
                toAdd = new ArrayList<>(res.getKey());
                toAdd.add(l.get(l.size() - 1));
                path.add(res.getKey());
            }
            if (currentAnswer < answer) {
                answer = currentAnswer;
                answerPath = path;
            }
        }

        return new Pair<>(answerPath, answer);
    }


    private Pair<List<String>, Double> findDistanceBetween(String node1, String node2) {
        double distance;
        try {
            distance = res.getDistance(node1, node2);
        } catch (Exception ex) {
            return null;
        }
        distance /= 100000;
        List<String> toRet = new ArrayList<>();
        Iterable<DirectedWeightedEdge<String, Integer>> path = res.getPath(node1, node2);
        path.forEach(e -> {
            toRet.add(e.from());
        });
        toRet.add(node2);
        return new Pair<>(toRet, distance);
    }

    private <E> List<List<E>> generatePerm(List<E> original) {
        if (original.isEmpty()) {
            List<List<E>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<>();
        List<List<E>> permutations = generatePerm(original);
        for (List<E> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<E> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }
}
