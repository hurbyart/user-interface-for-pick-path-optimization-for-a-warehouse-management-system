package hurbyart.simplifiedWMS.algorithms.dijkstra;

import java.util.List;

public class ResultNode {

    private Node node1;
    private Node node2;
    private double dist;
    private List<Node> path;


    public ResultNode(Node source, Node end, double dist, List<Node> path) {
        this.node1 = source;
        this.node2 = end;
        this.dist = dist;
        this.path = path;
    }

    public double getDist() {
        return dist;
    }

    public List<Node> getPath() {
        return path;
    }

    public Node getNode1() {
        return node1;
    }

    public Node getNode2() {
        return node2;
    }
}
