package hurbyart.simplifiedWMS.algorithms.dijkstra;

import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class DijkstraAlgorithmWithPermutation extends DijkstraAlgorithm {

    public DijkstraAlgorithmWithPermutation(NodeWMS spNode, List<NodeWMS> nodes, List<Arc> arcs) {
        super(spNode, nodes, arcs);
    }

    List<ResultNode> allDistances = new ArrayList<>();

    NodeWMS sourceWMS;
    NodeWMS goalWMS;
    List<NodeWMS> nodesWMSinOrder;

    public Pair<List<List>, Double> execute(NodeWMS source, NodeWMS goal, List<NodeWMS> nodesWMS) {
        this.sourceWMS = source;
        this.goalWMS = goal;
        this.nodesWMSinOrder = nodesWMS;
        Node sourceNode = null;
        Node goalNode = null;
        for (Node n : graph.getNodes()) {
            if (source != null && n.getName() == source.getId()) sourceNode = n;
            if (goal != null && n.getName() == goal.getId()) goalNode = n;
        }
        List<Node> nodes = new ArrayList<>();
        for (NodeWMS nodeWMS : nodesWMS) {
            for (Node node : graph.getNodes()) {
                if (nodeWMS.getId() == node.getName()) nodes.add(node);
            }
        }
        return run(sourceNode, goalNode, nodes);
    }

    /**
     * Method to create path using Dijkstra algorithm with permutation
     *
     * @param skus
     * @param useSpAsStart
     * @param useSpAsEnd
     * @return pair which contains arcs to create path and path length
     */
    public Pair<List<List<Arc>>, Double> createPath(List<Sku> skus, boolean useSpAsStart, boolean useSpAsEnd) {

        List<List<Arc>> arcsToDraw = new ArrayList<>();

        List<NodeWMS> nodesInOrder = new ArrayList<>();
        for (Sku sku : skus) {
            nodesInOrder.add(findNodesWithSlotId(sku.getSlotId()).get(0));
        }
        NodeWMS startNode = useSpAsStart ? spNode : null;
        NodeWMS endNode = useSpAsEnd ? spNode : null;
        Pair<List<List>, Double> res = execute(startNode, endNode, nodesInOrder);
        if (res.getKey() == null) return null;
        for (List<Node> l : res.getKey()) {
            List<Arc> arcs = new ArrayList<>();
            for (int i = 0; i < l.size() - 1; i++) {
                Arc arc = findArcByNodeIds(l.get(i).getName(), l.get(i + 1).getName());
                if (arc != null) arcs.add(arc);
            }
            arcsToDraw.add(arcs);
        }

        return new Pair<>(arcsToDraw, res.getValue());
    }

    private Pair<List<List>, Double> run(Node source, Node goal, List<Node> mustVisitNodes) {

        List<Node> importantNodes = new ArrayList<>();
        if (source != null) importantNodes.add(source);
        if (goal != null) importantNodes.add(goal);
        importantNodes.addAll(mustVisitNodes);

        for (int i = 0; i < importantNodes.size(); i++) {
            for (int j = 0; j < importantNodes.size(); j++) {
                if (i < j) {
                    allDistances.add(getShortestPathBetweenTwo(importantNodes.get(i), importantNodes.get(j)));
                }
            }
        }


        double answer = Double.MAX_VALUE;
        List<List> answerPath = null;
        List<List<Node>> permutationOfMustVisitNodes = generatePerm(mustVisitNodes);
        for (List<Node> l : permutationOfMustVisitNodes) {
            List<List> path = new ArrayList<>();
            double currentAnswer = 0.0;
            Pair<List<Node>, Double> res;
            List<Node> toAdd;
            if (source != null) {
                res = findDistanceBetween(source.getName(), l.get(0).getName());
                currentAnswer += res.getValue();
                toAdd = new ArrayList<>(res.getKey());
                toAdd.add(l.get(0));
                path.add(toAdd);
            }
            for (int i = 0; i < l.size() - 1; i++) {
                res = findDistanceBetween(l.get(i).getName(), l.get(i + 1).getName());
                currentAnswer += res.getValue();
                toAdd = new ArrayList<>();
                toAdd.add(l.get(i));
                toAdd.addAll(res.getKey());
                toAdd.add(l.get(i + 1));
                path.add(toAdd);
            }
            if (goal != null) {
                res = findDistanceBetween(l.get(l.size() - 1).getName(), goal.getName());
                currentAnswer += res.getValue();
                toAdd = new ArrayList<>(res.getKey());
                toAdd.add(l.get(l.size() - 1));
                path.add(toAdd);
            }
            if (currentAnswer < answer) {
                answer = currentAnswer;
                answerPath = path;
            }
        }


        return new Pair<>(answerPath, answer);
    }

    private Pair<List<Node>, Double> findDistanceBetween(int node1, int node2) {
        for (ResultNode resNode : allDistances) {
            if (resNode.getNode1().getName() == node1 && resNode.getNode2().getName() == node2
                    || resNode.getNode1().getName() == node2 && resNode.getNode2().getName() == node1) {
                return new Pair(resNode.getPath(), resNode.getDist());
            }
        }
        return null;
    }

    private ResultNode getShortestPathBetweenTwo(Node source, Node end) {
        Graph g = calculateShortestPathFromSource(source);
        Node n = g.getNodeById(end.getName());
        ResultNode res = new ResultNode(source, end, n.getDistance(), n.getShortestPath());
        cleanNodes();
        return res;
    }

    private <E> List<List<E>> generatePerm(List<E> original) {
        if (original.isEmpty()) {
            List<List<E>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<>();
        List<List<E>> permutations = generatePerm(original);
        for (List<E> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<E> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

    private void cleanNodes() {
        for (Node n : graph.getNodes()) {
            n.setShortestPath(new ArrayList<>());
            n.setDistance(Double.MAX_VALUE);
        }
    }
}
