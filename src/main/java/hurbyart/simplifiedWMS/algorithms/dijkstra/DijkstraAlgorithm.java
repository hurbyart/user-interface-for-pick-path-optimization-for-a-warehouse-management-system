package hurbyart.simplifiedWMS.algorithms.dijkstra;

import hurbyart.simplifiedWMS.algorithms.Algorithm;
import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import javafx.util.Pair;

import java.util.*;

public class DijkstraAlgorithm extends Algorithm {

    Graph graph = new Graph();

    public DijkstraAlgorithm(NodeWMS spNode, List<NodeWMS> nodes, List<Arc> arcs) {
        super(spNode, nodes, arcs);
        loadGraph();
    }

    private void loadGraph() {
        for (Arc arc : arcs) {
            Node node1 = null;
            Node node2 = null;

            for (Node node : graph.getNodes()) {
                if (node.getName() == arc.getNode1().getId()) {
                    node1 = node;
                }
                if (node.getName() == arc.getNode2().getId()) {
                    node2 = node;
                }
            }
            if (node1 == null) {
                node1 = new Node(arc.getNode1().getId());
                graph.addNode(node1);
            }
            if (node2 == null) {
                node2 = new Node(arc.getNode2().getId());
                graph.addNode(node2);
            }
            node1.addDestination(node2, arc.getTravelFactor());
            node2.addDestination(node1, arc.getTravelFactor());
        }
    }

    /**
     * Method to create path using Dijkstra algorithm
     *
     * @param skus
     * @param useSpAsStart
     * @param useSpAsEnd
     * @return pair which contains arcs to create path and path length
     */
    public Pair<List<List<Arc>>, Double> createPath(List<Sku> skus, boolean useSpAsStart, boolean useSpAsEnd) {
        // fill in map with skus and related them nodes
        List<List<Arc>> arcsToDraw = new ArrayList<>();
        double pathLength = 0.0;
        boolean pathWasntFound = false;
        long startAlgorithmTime = System.nanoTime();
        if (useSpAsStart) {
            List<NodeWMS> relatedNodesToSkuTo = findNodesWithSlotId(skus.get(0).getSlotId());
            for (NodeWMS nodeTo : relatedNodesToSkuTo) {
                graph = execute(spNode.getId());
                pathLength += graph.getNodeById(nodeTo.getId()).getDistance();
                addToArcsToDraw(arcsToDraw, graph, nodeTo.getId());
            }
        }
        for (int i = 0; i < skus.size() - 1; i++) {
            Sku skuFrom = skus.get(i);
            int slotIdFrom = skuFrom.getSlotId();
            List<NodeWMS> relatedNodesToSkuFrom = findNodesWithSlotId(slotIdFrom);
            Sku skuTo = skus.get(i + 1);
            int slotIdTo = skuTo.getSlotId();
            List<NodeWMS> relatedNodesToSkuTo = findNodesWithSlotId(slotIdTo);

            for (NodeWMS nodeFrom : relatedNodesToSkuFrom) {
                for (NodeWMS nodeTo : relatedNodesToSkuTo) {
                    graph = new Graph();
                    loadGraph();
                    graph = execute(nodeFrom.getId());
                    pathLength += graph.getNodeById(nodeTo.getId()).getDistance();
                    addToArcsToDraw(arcsToDraw, graph, nodeTo.getId());
                }
            }
        }
        if (useSpAsEnd) {
            List<NodeWMS> relatedNodesToSkuFrom = findNodesWithSlotId(skus.get(skus.size() - 1).getSlotId());
            for (NodeWMS nodeFrom : relatedNodesToSkuFrom) {
                graph = new Graph();
                loadGraph();
                graph = execute(nodeFrom.getId());
                pathLength += graph.getNodeById(spNode.getId()).getDistance();
                addToArcsToDraw(arcsToDraw, graph, spNode.getId());
            }
        }

        return new Pair<>(arcsToDraw, pathLength);
    }


    private Graph execute(int nodeId) {
        for (Node node : graph.getNodes()) {
            if (node.getName() == nodeId) {
                return calculateShortestPathFromSource(node);
            }
        }
        return null;
    }

    protected Graph calculateShortestPathFromSource(Node source) {
        source.setDistance(0);

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            Node currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Map.Entry<Node, Double> adjacencyPair :
                    currentNode.getAdjacentNodes().entrySet()) {
                Node adjacentNode = adjacencyPair.getKey();
                double edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.add(currentNode);
        }
        return graph;
    }

    private Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        double lowestDistance = Double.MAX_VALUE;
        for (Node node : unsettledNodes) {
            double nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private void calculateMinimumDistance(Node evaluationNode, double edgeWeigh, Node sourceNode) {
        double sourceDistance = sourceNode.getDistance();
        if (sourceDistance + edgeWeigh < evaluationNode.getDistance()) {
            evaluationNode.setDistance(sourceDistance + edgeWeigh);
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }


    private void addToArcsToDraw(List<List<Arc>> arcsToDraw, Graph g, int nodeToId) {
        for (Node node : g.getNodes()) {
            if (node.getName() == nodeToId) {
                List<Arc> arcs = new ArrayList<>();
                for (int j = 0; j < node.getShortestPath().size(); j++) {
                    Arc arc;
                    if (j == node.getShortestPath().size() - 1) {
                        arc = findArcByNodeIds(node.getShortestPath().get(j).getName(), nodeToId);
                    } else {
                        arc = findArcByNodeIds(node.getShortestPath().get(j).getName(), node.getShortestPath().get(j + 1).getName());
                    }
                    arcs.add(arc);
                }
                arcsToDraw.add(arcs);
            }
        }
    }
}
