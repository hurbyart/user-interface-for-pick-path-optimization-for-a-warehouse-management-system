package hurbyart.simplifiedWMS.algorithms;

import hurbyart.simplifiedWMS.warehouseObjects.Arc;
import hurbyart.simplifiedWMS.warehouseObjects.NodeWMS;
import hurbyart.simplifiedWMS.warehouseObjects.Sku;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public abstract class Algorithm {
    protected NodeWMS spNode;
    protected List<NodeWMS> nodes;
    protected List<Arc> arcs;

    public Algorithm(NodeWMS spNode, List<NodeWMS> nodes, List<Arc> arcs) {
        this.spNode = spNode;
        this.nodes = nodes;
        this.arcs = arcs;
    }

    protected Arc findArcByNodeIds(int nodeId1, int nodeId2) {
        for (Arc arc : arcs) {
            if (arc.getNode1().getId() == nodeId1 && arc.getNode2().getId() == nodeId2) return arc;
            if (arc.getNode1().getId() == nodeId2 && arc.getNode2().getId() == nodeId1) return arc;
        }
        return null;
    }

    protected List<NodeWMS> findNodesWithSlotId(int slotId) {
        List<NodeWMS> toRet = new ArrayList<>();
        for (NodeWMS node : nodes) {
            if (node.getSlot() != null && node.getSlot().getId() == slotId) {
                toRet.add(node);
            }
        }
        return toRet;
    }

    public abstract Pair<List<List<Arc>>, Double> createPath(List<Sku> skus, boolean useSpAsStart, boolean useSpAsEnd);
}
