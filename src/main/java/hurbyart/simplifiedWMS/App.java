package hurbyart.simplifiedWMS;

import hurbyart.simplifiedWMS.panels.PanelController;
import hurbyart.simplifiedWMS.panels.PanelMode;
import javafx.application.Platform;

/**
 * This is the main class of application created for bachelor thesis "User interface for pick path optimization for a Warehouse Management System"
 *
 * @author Artem Hurbych(hurbyart@fel.cvut.cz)
 * <p>
 * During application implementation psjava library was used. Here is its license:
 * <p>
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2013 psjava authors
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
public class App {

    public static void main(String[] args) {
        Platform.setImplicitExit(false);
        PanelController menu;
        if (args.length == 1) {
            menu = new PanelController(args[0]);
        } else {
            menu = new PanelController(null);
        }
        menu.start(PanelMode.MENU);

    }
}