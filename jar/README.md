This is directory with jar of application bachelor thesis "User interface for pick path optimization for a Warehouse Management System".

You can start it using start_linux.sh on Linux. You need Java 11 or higher to start application.

To load from database mentioned in thesis use its name as argument. For example:

bash start.sh thirdPlan

If argument is not equals to mentioned database. Application will be started with default database.

